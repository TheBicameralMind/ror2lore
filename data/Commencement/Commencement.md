---
title: Commencement
image: /img/Commencement.png
tags:
- Environments
---

{{< figure src="/img/Commencement.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">Have you forgotten – that those gates are my design?

You would not know, because you do not look up. You bend knees only for ants and vermin. But I have been working on a grander design. A greater gate – to travel greater seas.

What if I met you, on one of these extinct planets? What if I caused the calamities you strive to avoid? What if I reaped destruction? What would you do then?

I know the planet you trail – of water and dirt. You fear their stability. That they will consume themselves. Maybe I will go there first.</pre>
{{< /rawhtml >}}

