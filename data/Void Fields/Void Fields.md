---
title: Void Fields
image: /img/Void_Fields.png
tags:
- Environments
---

{{< figure src="/img/Void_Fields.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">The cells were for dangerous creatures.

The cells were for rare, powerful relics of exponential strength.

The cells were for ancient automata of war.

The cells were for scientists and inventors and explorers.

The cells were for gods.

The cells were for everything.</pre>
{{< /rawhtml >}}

