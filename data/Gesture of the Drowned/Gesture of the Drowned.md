---
title: Gesture of the Drowned
image: /img/Gesture_of_the_Drowned.png
tags:
- Utility Items
---

{{< figure src="/img/Gesture_of_the_Drowned.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Fossils. Remnants. How cruel. 

This moon once housed life. Life that you may have held dear, had the timing been right. But our timing was wrong. We were born much too late.

Now, it is just calcium to me – and irrelevant to you. Isn’t that right?</pre>
{{< /rawhtml >}}

