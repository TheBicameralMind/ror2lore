---
title: Topaz Brooch
image: /img/Topaz_Brooch.png
tags:
- Utility Items
---

{{< figure src="/img/Topaz_Brooch.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Topaz Brooch
Tracking Number: 442*****
Estimated Delivery: 05/05/2056
Shipping Method: High Priority
Shipping Address: Locker #2, Parghos Resort, Venus
Shipping Details:

The locker combination is 33941. Inside the package, besides your standard equipment, you will find the brooch. The appearance of this item is meant to blend in at the party. Pin it to your body and the software inside will map your extents. Activation will briefly cover you in ultra-phasic shielding.

When you have taken out the target, the shield will switch on to protect you from any immediate retaliation measures they have installed in the security system. If the time frame is not enough to make your escape, the shield can be... reactivated, but we’d like to keep casualties to a minimum.

Good luck.</pre>
{{< /rawhtml >}}

