---
title: Bundle of Fireworks
image: /img/Bundle_of_Fireworks.png
tags:
- Damage Items
---

{{< figure src="/img/Bundle_of_Fireworks.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Hey, happy anniversary!

You really thought I forgot?

Ah, I thought I was the dumb one in our relationship. C'mon T, of course not! Well, I wasn't going to let a little alien death planet ruin our night. Right? Shh, just watch. Just watch. It's okay. Just watch. You'll love it - you really will. Then we can go home.

You can keep your eyes closed. Just listen. Ah - here it comes! In five... four... three... two... ONE...!"</pre>
{{< /rawhtml >}}

