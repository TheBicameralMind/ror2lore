---
title: Ghor's Tome
image: /img/Ghor's_Tome.png
tags:
- Utility Items
---

{{< figure src="/img/Ghor's_Tome.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: “Ghor’s Tome”
Tracking Number: 19******
Estimated Delivery: 07/07/2056
Shipping Method: Priority
Shipping Address: 99th Floor, Crumbling Tower, Venus
Shipping Details:

Ancient secrets... transmutation... homunculi... alchemy. Nothing more than nonsense - pursuits of rightly intentioned but grossly misled minds. Or so I thought. This item's... unexpected physical properties are reason enough to stay my immediate dismissal.

Strange fortune led this book to me, and stranger knowledge still fills the pages. Chilling, even. It concludes, in chapter eight, that even iron can become gold - but only through a ritual of intense bloodletting, a thought disturbing enough without considering the bountiful riches that accompanied the estate.

The takeaway, above all else, seems to be that you cannot get something from nothing; even the very paper of the tome turned to ash as I tried to make photocopies. Whatever was on the first two pages is lost forever. I need transcriptions of the rest by hand, and I know you take pride in your precision. 

And more importantly, Kosta, I trust you. Don't do anything that would draw attention to yourself. I will try to find the other volumes. This is knowledge too dangerous to lose.</pre>
{{< /rawhtml >}}

