---
title: Unstable Tesla Coil
image: /img/Unstable_Tesla_Coil.png
tags:
- Damage Items
---

{{< figure src="/img/Unstable_Tesla_Coil.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Tesla Presentation Software v1.14
Powering on...
10...
Issuing welcome statement...
9...</span>
Welcome one and all!
<span class="mono">8...</span>
Please take a seat.
<span class="mono">7...</span>
Make sure those behind you can see.
<span class="mono">6...</span>
The presentation will start shortly.
<span class="mono">5...</span>
Please obey the staff for your safety.
<span class="mono">4...
Initiating room mood lighting...
3...</span>
Get ready to behold...
<span class="mono">2...</span>
The marvelous wonders...
<span class="mono">1...</span>
Of electricity!
<span class="mono">Power anomaly detected...
Initiating reboot procedure in 10...</span></pre>
{{< /rawhtml >}}

