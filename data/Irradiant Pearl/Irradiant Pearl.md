---
title: Irradiant Pearl
image: /img/Irradiant_Pearl.png
tags:
- Damage Items
---

{{< figure src="/img/Irradiant_Pearl.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Oh, that is… strange. That is unlike the other pearls.

Do not be mistaken – his influence is still dark. Despite how beautiful your pearl is… it is not from a place of kindness. It cannot be.</pre>
{{< /rawhtml >}}

