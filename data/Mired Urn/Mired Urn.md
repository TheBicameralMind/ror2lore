---
title: Mired Urn
image: /img/Mired_Urn.png
tags:
- Healing Items
---

{{< figure src="/img/Mired_Urn.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>
Subject: Mired Urn
Technician: J. Flint
Table Spec: Full Enclosure AY-2 with ARMM Kit
Notes:

- The survey team went through a lot of trouble to bring this one back.
- Fitz was the only one left standing, in fact said he felt great, and the rest are still in the recovery ward
- Subject is what appears to be a clay vase
- Could this be part of a camouflaging defense mechanism?
- Inside the vase is a dark gooey substance
- Goo can articulate into functioning appendages
- Intelligence of subject indeterminable at this point
- When operating the ARMM kit, goo attaches to all but one of them
- Instability in the ARMM power system aborts the procedure</pre>
{{< /rawhtml >}}

