---
title: Red Whip
image: /img/Red_Whip.png
tags:
- Utility Items
---

{{< figure src="/img/Red_Whip.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“...going to 594 South River, Io. Check. One Whip, Red, Priority shipping. Going to... to, um... Oh, wow...”

-Signal echoes, UES Contact Light</pre>
{{< /rawhtml >}}

