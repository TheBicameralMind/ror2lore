---
title: Genesis Loop
image: /img/Genesis_Loop.png
tags:
- Damage Items
---

{{< figure src="/img/Genesis_Loop.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>

Subject: Genesis Loop
Technician: Eleanor Veirs
Table Spec: Dissection 5B4 with Ele-Maintenance Kit
Notes: 

&gt; Genesis Loop is highly conductive organ, contains conductive filaments stretching from one end to the other
&gt; Both sides of loop have organelles at end; one has positive charge, one has negative charge
&gt; Sticking ends of loop together generates voltage
&gt; Connected loop and fed organ 50 volts
&gt; Hair stood up on end, touching loop resulting in numbness in fingers
&gt; Upping voltage to 100 volts
&gt; Shooting arcs from organ, loop shudders and glows bright, dissonant humming is heard
&gt; Upping voltage to 200 volts
&gt; Bright flash, a loud crack as loop releases a blast that knocks me back
&gt; Lights go out
&gt; Dissection Table hardware damaged by voltage
&gt; I hope I don’t get in trouble for this</pre>
{{< /rawhtml >}}

