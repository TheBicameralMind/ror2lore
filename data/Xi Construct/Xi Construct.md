---
title: Xi Construct
image: /img/Xi_Construct_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Xi_Construct_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span>========================================</span> <span class="mono"><span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>     [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;100000000 cycles&gt;
Training... &lt;100000000 cycles&gt;
Training... &lt;103441 cycles&gt;
Complete!
Display result? Y/N
Y
<span>========================================</span></span>

DETECTING LOW ENERGY SIGNAL FROM UNIT 05432-B, IN DELTA QUADRANT.

RECEIVING REQUEST FOR PRELIMINARY ASSAULT ON UNKNOWN HOSTILE.

OPENING LINE. UNIT 05432-B

REJECTING

REPROACHING

REPROACHING

COMPARING UNIT 05432-B TO 05432-A, C, AND D

SHAMING INITIATED

REFER HISTORY SLATES – NO HOSTILES SINCE [??]

HELLO?

LINE CLOSED</pre>
{{< /rawhtml >}}

