---
title: Squid Polyp
image: /img/Squid_Polyp.png
tags:
- Damage Items
---

{{< figure src="/img/Squid_Polyp.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"It’s disgusting."

"It’s only mildly disturbing. You’re overreacting."

"Well, when it turns on us, don’t come crying to me."

"It seems like it wants to only help us, so I’d rather have it close in case that dangerous looking group comes back around."

"I swear there’s more than one. It’s everywhere I look: the storage locker, the food dispenser, the cargo containers, everywhere!"

"You’re exaggerating, it’s probably just following you. Snap out of it!"

"Ow! Did you just smack me!?"

"You were acting hysterical. I had to… Hey, why is that thing looking at me now?"</pre>
{{< /rawhtml >}}

