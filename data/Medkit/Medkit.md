---
title: Medkit
image: /img/Medkit.png
tags:
- Healing Items
---

{{< figure src="/img/Medkit.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">"I suggest you stay indoors and rest."</span>

MED-E, the medical bot, has been working overtime. The survivors felt very grateful that their escape pod landed near Med Bay cargo - they would have most likely perished without it.

<span class="mono">"I suggest you stay indoors and rest."</span>

The robot was speaking to a tall man, gaunt but strong. "I'd love to, but we're running low on food." Even though his expression was hidden underneath his blue combat helmet, it was obvious the man was extremely tired. "And everyone else is recovering from last night's attacks. Without the enforcers, we probably wouldn't have-"

<span class="mono">"I suggest you stay indoors and rest." </span>

It was apparent that MED-E only had a limited set of lines. However, the man continued to respond. "I can't, MED-E. They're counting on me. You know that."

<span class="mono">"I suggest you stay indoors and rest." </span>

This time the man did not humor a reply. Grabbing his shotgun, he turned away from the echoing robot. Weaving his way between countless sleeping bags and stretchers, he set out into the wilderness again. Everyone was counting on him.

<span class="mono">"Please stay indoors and rest." </span></pre>
{{< /rawhtml >}}

