---
title: Elder Lemurian
image: /img/Elder_Lemurian_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Elder_Lemurian_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">FIELDTECH Image-To-Text Translator (v2.5.10b)
# Awaiting input... done.
# Reading image for text... done.
# Transcribing data... done.
# Translating text... done. [12 exceptions raised]
Complete: outputting results.</span>

Saved by [the hero], our paradise was one of sprawling tunnels and rich caves. Making our home in the depths of paradise, we began constructing a great civilization. 

The elders constructed elaborate living quarters, decorated with tetrafoil and glass baubles, and our little ones began charting paths through the tunnels. [The hero] was honored for [his] kindness, and we constructed a great temple on paradise’s surface in [his] honor. Song and dance rang out through the tunnels for weeks to come in celebration for our salvation.

<span class="mono">Translation Errors:</span>
# [The Hero] could not be fully translated.</pre>
{{< /rawhtml >}}

