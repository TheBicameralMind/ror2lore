---
title: Backup Magazine
image: /img/Backup_Magazine.png
tags:
- Utility Items
---

{{< figure src="/img/Backup_Magazine.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Ammo Magazine, Standard Ammunition (45mm Rounds)
Tracking Number: 05***********
Estimated Delivery: 10/05/2056
Shipping Method:  Priority
Shipping Address: Cargo Bay 10-C, Terminal 504-B, UES Port Trailing Comet
Shipping Details: 
- Billed to: Captain [REDACTED]
- Note from Sender:  You going on a hunting trip or something? I’ve never seen anyone order this much ammo before.</pre>
{{< /rawhtml >}}

