---
title: Voidsent Flame
image: /img/Voidsent_Flame.png
tags:
- Damage Items
---

{{< figure src="/img/Voidsent_Flame.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span>========================================</span>
<span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>     [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;100000000 cycles&gt;
Training... &lt;100000000 cycles&gt;
Training... &lt;102515 cycles&gt;
Complete!
Display result? Y/N
Y
<span>========================================</span>

We take kindling and throw it in the fire.

The fire is pleased. The fire grows.

We take fruit and throw it in the fire.

The fire loves this gift. Embers pop from the fire and perform dances of joy before fading. Compounds inside the fruit are undone, returning to their base components. There are so many colors. So much energy.

We take a body and throw it into the fire. It will not be missed. He has more to spare.

The fire loves this gift. Fat in the body burns, producing a rainbow of beautiful hues in the flame. Muscles contract and snap, stimulated by the gnawing heat. The fire claims its meal, and returns it to energy.

So much energy. I believe I can work with this.</pre>
{{< /rawhtml >}}

