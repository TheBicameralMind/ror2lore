---
title: Captain
image: /img/Captain.png
tags:
- Survivors
---

{{< figure src="/img/Captain.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“A rescue mission?”

The old man leaned forward, scanning the document slid over his desk. His eyes paused on a collection of words near the bottom: UES Contact Light.

“Yes.”

He continued to read the document. The Safe Travels? For a rescue mission? As one of the few surviving captains of the old colony ships, he knew all the designations by memory – and the Safe Travels was not a rescue ship.

“Any armaments?”

“No.”

“We’ll have to fix that.”

With a sigh, he stood up. His prosthetics tugged at his joints – despite being lighter than his original limbs, they always felt heavy.

“And let me guess - top secret, right?”

“Yes.”

He gave out another sigh – they’re always top secret - but a small smile crept onto his face. He glanced up to his old radio helmet. He’s been bored in retirement anyways.</pre>
{{< /rawhtml >}}

