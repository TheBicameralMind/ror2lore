---
title: Soulbound Catalyst
image: /img/Soulbound_Catalyst.png
tags:
- Utility Items
---

{{< figure src="/img/Soulbound_Catalyst.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Soulbound Catalyst
Tracking Number: 890*****
Estimated Delivery: 11/06/2056
Shipping Method: High Priority/Fragile
Shipping Address: 1414 Place, Fillmore, Venus
Shipping Details:

If you thought the ring was a rare find, wait until you get your hands on this!

It turns out the method the King used did not originate from his time period. His court discovered a ritual performed by a tribe from much further back. It, of course, dealt with sacrifice, but in substantial groups. This was supposed to concentrate the catalyzing properties of the item it was imbued into.

Payment will be at least twice as much as our previous deal. We can discuss it later - another night.</pre>
{{< /rawhtml >}}

