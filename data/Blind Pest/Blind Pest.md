---
title: Blind Pest
image: /img/Blind_Pest_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Blind_Pest_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
 $ Combing for relevant data... done.
 Complete!

</span>This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

 While hunting a group of Blind Vermin, Tharson and I were ambushed by a group of creatures that heavily resembled the Vermin we had been hunting. Their major difference, of course, was their ability to fly! I have assigned their common name as "Radkari Blind Pest".

* Much like their earthbound cousins, the Pests are blind and rely heavily on their smell and hearing. However, their pronounced tongue may indicate that they may use taste to identify things as well.

* The Pests' primary characteristic is their ability to fly. Alongside this, they also weaponize their acidic bile as a projectile attack. This could allow them to partially digest their prey as they hunt... How fascinatingly foul.

* They typically inhabit warmer, damper areas, like jungles. After our skirmish, I noted nests in the canopy. This indicates that the Pests are ambush predators, waiting for prey to waltz below their nests.</pre>
{{< /rawhtml >}}

