---
title: Power Elixir
image: /img/Power_Elixir.png
tags:
- Healing Items
---

{{< figure src="/img/Power_Elixir.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: 16 oz. Flask, Healing Potion (Not my Strongest)
Tracking Number: 10******
Estimated Delivery: 12/31/2058
Shipping Method: Priority
Shipping Address: Cargo Bay 10-C, Terminal 504-B, UES Port Trailing Comet
Shipping Details: 
- Re: Potion seller, I am going into battle. I require your strongest potions.

My potions are too strong for you, buyer. I've instead downgraded you to a weaker brew. My strongest potions would kill a dragon, let alone a man!</pre>
{{< /rawhtml >}}

