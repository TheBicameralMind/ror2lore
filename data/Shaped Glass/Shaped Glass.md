---
title: Shaped Glass
image: /img/Shaped_Glass.png
tags:
- Damage Items
---

{{< figure src="/img/Shaped_Glass.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono"><span>========================================</span>
<span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>     [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;100000000 cycles&gt;
Training… &lt;100000000 cycles&gt;
Training… &lt;50912157 cycles&gt;
Paused…
Display partial result? Y/N
Y
<span>================================</span></span>
Pairings

Unstructured glass, from the heart of the [Moon]. Sung out, in ethereal wisps, over the course of 3 cycles. Pause.

We fold time into its material - twice. Our time and &lt;his&gt;. A cost &lt;he&gt; was willing us to pay. Folded and shaped, with a god's designs.
 
&lt;He&gt; wields it, in one of many great hands. The time we injected is unfolded in &lt;his&gt; grasp. Outputs quicken - muscles compress twice. Twice as many intentions. Twice the ordered complexities, folded upon themselves. Loops loop back onto [?] in pairs. Time dependent functions. Pause.

&lt;He&gt; sunders a construct into a thousand pieces.

But time is fair. Microtears begin to uncoil in pairs. Muscles begin to snap, twice as fast. The cost of folded time. But &lt;he&gt; has plenty of time.

<span class="mono">
<span>================================</span>
Continue training? Y/N
Y</span></pre>
{{< /rawhtml >}}

