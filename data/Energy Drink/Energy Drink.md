---
title: Energy Drink
image: /img/Energy_Drink.png
tags:
- Utility Items
---

{{< figure src="/img/Energy_Drink.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: NR-G Sports Soda (400)
Tracking Number: 49******
Estimated Delivery: 03/12/2056
Shipping Method:  Standard
Shipping Address: Venetian Health &amp; Public Services, Ridgelake, Venus
Shipping Details:

Yeah, so, uh this is the um, the energy drink that people were mixing during that disaster in Majora Minora a few weeks ago. 

A quick toxicology test on some of the victims has come back positive for um... all kinds of stuff, really. Methadone, phencyclidine, tetrahydrocannabinol, a variety of opiates, benzodiazepines, barbiturates, amphetamines... the list just goes on and on and on.

Radical Drinks is insisting that NONE of those were from their energy drink - which I find pretty uh, unlikely. Maybe they REALLY know how to party in Violet Heights, huh?</pre>
{{< /rawhtml >}}

