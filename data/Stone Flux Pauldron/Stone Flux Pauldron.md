---
title: Stone Flux Pauldron
image: /img/Stone_Flux_Pauldron.png
tags:
- Utility Items
---

{{< figure src="/img/Stone_Flux_Pauldron.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Much like the earth, you must be sturdy and inflexible. Completely and utterly immovable, in both body and mind. Fear not the sword of your enemy, visualize it shattering against your skin. Focus, and make it true. Fear not the tumbling avalanche, visualize it yielding to your will. But heed, and do not fight without reason. Those who remain rooted will quickly find themselves alone."

-Will of Combat, First Excerpt</pre>
{{< /rawhtml >}}

