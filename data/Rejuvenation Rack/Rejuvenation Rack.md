---
title: Rejuvenation Rack
image: /img/Rejuvenation_Rack.png
tags:
- Healing Items
---

{{< figure src="/img/Rejuvenation_Rack.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Nature has a way of nurturing the physical. The mind, by perseverance and dedication. The soul, however... is healed by fantasy, and fantasy alone."
-Unknown Venetian monk</pre>
{{< /rawhtml >}}

