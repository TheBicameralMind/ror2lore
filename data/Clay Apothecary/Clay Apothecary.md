---
title: Clay Apothecary
image: /img/Clay_Apothecary_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Clay_Apothecary_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">"The Tar's history is unimportant. What well or crevice it first flowed from doesn't matter; only that the Tar flows.

And so it did. Through its vessels the Tar spread, consuming its homeworld entirely. But the Tar still hungered.

Riding down on a falling star, the Tar spread to the unsuspecting world of Aphelia.

The Tar offered its power to the dunepeople, promising them energy and sustenance. The dunepeople, hungry and battered from life in the harsh desert, accepted.

The Tar didn't ask for much. The Tar only asked that the dunepeople spread it across Aphelia, so the Tar may feed. And so it did; the dunepeople crafted elaborate clay vessels to carry the Tar to and fro. Mighty aqueducts, belching forth black rivers of Tar, dotted the skyline. Those who pleased the Tar were blessed with its gift; a connection. They were one with the Tar.

And it was in this reverence that the dunepeople began to look back. The lush riversides, consumed. The animals that served as livestock and steed, drowned in Tar. The Tar had consumed everything, even the dunepeople themselves. They were nothing more than extensions of the Tar's will.

And when the Tar asked for more, the dunepeople had nothing more to give.

Only a mighty hero, riding down from heaven on a glowing blue dragon, offered the dunepeople a chance at salvation. Desperate, the dunepeople took to the hero's arms, and began their exodus to paradise.

And the Tar continued to spread."

- Tragedy of Aphelia</pre>
{{< /rawhtml >}}

