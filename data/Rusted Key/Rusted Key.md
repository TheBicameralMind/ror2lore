---
title: Rusted Key
image: /img/Rusted_Key.png
tags:
- Utility Items
---

{{< figure src="/img/Rusted_Key.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Key
Tracking Number: 12******
Estimated Delivery: 01/21/2056
Shipping Method: Standard
Shipping Address: |||||||, Druid Hills, Earth
Shipping Details:

I don't trust UESC. Not one bit. Their Security Chests? Full of overrides and backdoors- I've got a cousin working in their factory, and they've got all kinds of weird things going on in those chests. I've seen so many of them in auctions - for the lost and unclaimed ones - and you just pay money, and it springs open on the spot. Are you kidding me?

Anyways, I'm sending you exactly what I said I would - but it's too important to leave the security up to the UESC. So I'm sending the key to you - and the lockbox to Margaret. Like a two-factor authentication. Let me know when you get this.</pre>
{{< /rawhtml >}}

