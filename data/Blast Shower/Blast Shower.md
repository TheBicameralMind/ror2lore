---
title: Blast Shower
image: /img/Blast_Shower.png
tags:
- Equipment
---

{{< figure src="/img/Blast_Shower.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Blast Shower
Tracking Number: 152*****
Estimated Delivery: 06/19/2056
Shipping Method: Standard
Shipping Address: Frontier Gate, Outer Edge Zone
Shipping Details:

Outer Edge thanks you for your rewards redemption! It takes a real explorer to make their living on the frontier. As you undoubtedly know, REAL exploring is REAL messy. This portable unit will equip you with the modern convenience of taking a shower anywhere at any time!

Disclaimer: Wear your protective suit while cleansing. Do not use product directly on skin. Blast Shower is only rated for B-class toxins and C-class foreign organisms. This company does not accept any responsibility for incidents as a result of an incomplete cleansing.</pre>
{{< /rawhtml >}}

