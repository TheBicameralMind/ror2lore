---
title: Old War Stealthkit
image: /img/Old_War_Stealthkit.png
tags:
- Utility Items
---

{{< figure src="/img/Old_War_Stealthkit.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order:  Misc. OW Artifacts (47)
Tracking Number: 01******
Estimated Delivery: 08/13/2056
Shipping Method: Priority
Shipping Address: National Old War Museum, New Orleans, Earth
Shipping Details:

We've managed to round up all the local Old War artifacts that we could find. A lot of interesting things here: rifles, rations, pamphlets. We even found a prototype stealthkit in fantastic condition; that could be a great centerpiece for your exhibit.

However, a slight issue: in transit, the truck hit a pothole and jostled all the ship. It seemed to have actually activated the stealth drive - some strange short-circuit - and our team can't actually confirm the location of the stealthkit. It should be in one of the boxes - we will be expecting the commission to include the stealthkit.</pre>
{{< /rawhtml >}}

