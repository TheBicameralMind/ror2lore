---
title: Lunar Chimera (Wisp)
image: /img/Lunar_Chimera_(Wisp)_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Lunar_Chimera_(Wisp)_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">Soul driven. Of course. As with all vermin - let us begin with fire.

Free axes of movement – wonderful design. I will highlight that.

Slow acceleration – that can be improved. Speed is war.

Feeble combat skills. Treatable. Those Templars may be of inspiration.</pre>
{{< /rawhtml >}}

