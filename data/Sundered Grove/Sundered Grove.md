---
title: Sundered Grove
image: /img/Sundered_Grove.png
tags:
- Environments
---

{{< figure src="/img/Sundered_Grove.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">Rings? What for?

A triangle is much more of a beautiful design. Look, and observe: three points, in three dimensions. No redundancies. Perfectly defined. We will use triangles.

Trees? Yes, trees are beautiful. I agree. We will have many trees.</pre>
{{< /rawhtml >}}

