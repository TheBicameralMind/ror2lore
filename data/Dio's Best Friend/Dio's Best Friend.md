---
title: Dio's Best Friend
image: /img/Dio's_Best_Friend.png
tags:
- Utility Items
---

{{< figure src="/img/Dio's_Best_Friend.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Hey Dio,

Wow, you have gotten older since the last time I wrote! How old are you now – 10? And so much grumpier, too. I’ve never seen a cat so grumpy. What do you have to be grumpy about?

Despite the fact that you are the pickiest eater I know – and love to meow all throughout the night, every night – it still makes me happy to hear you bounding down the stairs every time I come home. I like when I wake up and I see you sleeping across from me. I like when you sit at the dining table, like you’re going to eat with us too. I like how you hit the power button every time you walk across my laptop. I like how shamelessly needy you are.

Please live a long life – I’m needy too, and I’d like that a lot.

Love,
Duncan

PS would it kill you to purr for mama every now and then?</pre>
{{< /rawhtml >}}

