---
title: Defense Nucleus
image: /img/Defense_Nucleus.png
tags:
- Damage Items
---

{{< figure src="/img/Defense_Nucleus.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Excellent!  How did you find it?"

"We took down one of those floating fortune tellers<span>--</span>"

"Xi Construct."

"Sure.  Anyway, we took it down and recovered the sample."

"May I see it?"

"This is it."

"This?  What happened?  I thought you said it was glowing."

"That's right.  It was.  Before I scrapped it."

"...You ...what?"

"I scrapped it."

"You were finally able to recover a sample from the Xi Construct, and you scrapped it?  Do you realize monumental importance of the samples we've been gathering?"

"I realize the monumental danger.  I've read the lab files.  Daimera's crippled in one hand.  Veirs was THIS close to electrocuting herself.  After Fitz nearly KILLED his ENTIRE team for a VASE, you think I'm going to haul a mysterious glowing orb back here?  HELL.  NO."

"...Well, what's done is done.  I guess I'll analyze what's left.  Anything else?"

"Yeah, I was thinking we could call it a 'Defense Nucleus'."

"..."

"In the file."

"No."</pre>
{{< /rawhtml >}}

