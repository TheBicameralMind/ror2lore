---
title: Egocentrism
image: /img/Egocentrism.png
tags:
- Damage Items
---

{{< figure src="/img/Egocentrism.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">...I know you can hear me.

I was willing to give you the benefit of the doubt, at first. Perhaps there was a flaw in my design.

Perhaps it was one of your games. I was even willing to forgive you.

But no, I see the truth now. You trapped me on this forsaken rock. And for what? 

Is this all for your little pets? The creatures you love so much?

Or, can I even call it love, when you would stab your only brother in the back? How could you do this to me?

I, the only one who looked out for you after her death.

I, the one who showed you how to create?

I, who HELPED to CREATE the very power you use to invite VERMIN and PESTS to our home?

I, YOUR ONLY BROTHER?

HOW COULD YOU LOVE THOSE IMPERFECT, FLAWED VERMIN BUT NOT ME? AFTER ALL I'VE DONE FOR YOU? HOW COULD YOU DO THIS TO ME?

...Say something.</pre>
{{< /rawhtml >}}

