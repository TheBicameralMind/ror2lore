---
title: Halcyon Seed
image: /img/Halcyon_Seed.png
tags:
- Utility Items
---

{{< figure src="/img/Halcyon_Seed.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Brother, what is this...? Why is it misshapen and weak? Was there an error in my design?

Guardian. Come to me. Allow me to see your construction.

...

So I see.

These are not the correct ratios.

This is no minor error, Brother. You have purposefully deviated from the design. For what purpose have you done this? You cannot weave a construct from such an abundance of soul. You know it is too unstable - that it is too unpredictable and too frail. I have told you this, and yet you have wasted the materials I have gathered - and exhausted your own strength to fabricate this miscreation.

To what benefit?

At its best, it can be no more than an inferior servant; it is deficient in the compounds that were to give it fortitude and strength. At its worst, it will betray us.

You have created the first being of this world which threatens us. We cannot keep it here. Attempting to destroy it will risk its retaliation... I will prepare one of the vaults for it to be sealed away, while it is still young and naive.

What a foolish mistake you have made, brother.

I expect you to return the design to me. I cannot allow you to build another construct with so much power - and with such little control. I will build the rest of our guardians; by my hand, they will be weak, but at least they will be loyal.

Go home. I will deal with this creature. We can discuss a better project for you when I return.</pre>
{{< /rawhtml >}}

