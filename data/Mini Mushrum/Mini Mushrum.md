---
title: Mini Mushrum
image: /img/Mini_Mushrum_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Mini_Mushrum_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

On a routine food gathering trek we happened upon a vast fungal grove with towering fungi. Thinking we would take some samples back to test if they were edible, we noticed some of the smaller variety sprout up from the ground and circle around us.

I will describe its properties below. I have assigned their common name as ‘Mini Mushrum’.

• ‘Mini’ is a relative term here, as these mobile mushrooms were larger than Tharson. The subtle glow on the underside of their cap is probably to scare away predators, advertising that they are dangerous to eat.

• Waddling around on short, stubby legs makes them seem quite unsuited for walking. They comically bump into things as they fret about. Perhaps their eyeseight is poor from living in the dark?

• After a long time studying their movements, I suggested to Tharson to retrieve a sample from one to take back with us. The one that the sample was taken from began to flee as the others began shaking, spilling their spores into the air. Our air filtration warnings went off so we left before the cloud of fungal gas became too overwhelming. We did not eat the sample.</pre>
{{< /rawhtml >}}

