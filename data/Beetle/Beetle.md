---
title: Beetle
image: /img/Beetle_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Beetle_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

We have encountered more outer life - what a wonderful surprise! As we scavenged for food, the ground began to shift, and many large insect-like creatures began to burrow out from the ground. 

I will describe its properties below. I have assigned their common name as ‘Worker Beetle’.

• About the size of a small cow. Quadruped, with two large front-limbs. Moves similar to an earthen gorilla. Five eyes.

• The Beetle has two armor-like chitin plates on its head and back, presumably for defense. It seems to secrete a waxy substance.

• Beetles appear to be social in nature, with a hierarchy I cannot discern. I witnessed a group of 4 beetles resting under a tree, when a stray fifth came around. They promptly sprang up and repeatedly head-butted the creature until it left, battered and bruised.

• Tharson has reported to me that the Beetles are highly aggressive and territorial.</pre>
{{< /rawhtml >}}

