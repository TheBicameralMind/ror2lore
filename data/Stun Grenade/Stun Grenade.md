---
title: Stun Grenade
image: /img/Stun_Grenade.png
tags:
- Utility Items
---

{{< figure src="/img/Stun_Grenade.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM CARGO BAY 2 OF UES [Redacted] <span>--</span>//</span>
“Inventory duty. I swear, this must be the most boring position ever...” Two crewmates stood in the cargo bay, doing a routine check on the items gathered from the Contact Light’s wreckage. “Some of us are out there having adventures and fighting aliens, and I’M stuck in here counting dusty knick-knacks. What a bummer.” The other crewmate idly tapped at their device as their colleague rummaged through a container of recovered items.

“Can’t agree there. The Contact Light had millions of items in transit... It’s like a collage of people’s interests from all across the galaxy.” The other crew member scoffed. “Oh yeah? That’s really inspiring. Say, keep telling those stories, maybe it’ll make this go by quicker.”

“You just need to look at this from a different angle. There’s so much cool stuff in here! Like, for example...” The worker took something out of a container – glancing over at their colleague, the other worker stiffened.

“What the—is that a grenade!?” 

The worker laughed, tossing the dusty and dirty grenade from hand to hand. “Yeah. Don’t worry, thing was damaged in the crash. See? Completely harmless.” Holding the grenade up, a large dent was visible in its hull. “Man, I wonder what kind of model this is... I was always super interested in like, weaponry and stuff. Hm... looks to be a flashbang... old model, they don’t make these things anymore.” 

The other worker was not so thrilled. “I still don’t think you should play with that. That’s not a toy.” Squinting, the other worker realized in horror that the defunct grenade had no pin. “D-DUDE! Put that thing down, there’s no pin!”

“Huh? Oh, come on! Stop being such a buzzkill! This thing is busted beyond repair. It can’t even detonate, not even if I were to say... bang it against this table. Watch, I’ll show you.”

They were both admitted to the medical bay shortly after.</pre>
{{< /rawhtml >}}

