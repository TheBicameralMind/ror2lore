---
title: Purity
image: /img/Purity.png
tags:
- Utility Items
---

{{< figure src="/img/Purity.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">It looks like starseed, tanning in dirt.
It smells like hot stone, and tall grass.
It tastes like spiced fruit, sweet and hot.
It feels like solar winds, and solar chimes.
It sounds like two brothers, chasing glass frogs in the sun.</pre>
{{< /rawhtml >}}

