---
title: Brass Contraption
image: /img/Brass_Contraption_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Brass_Contraption_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

I am pleased to announce that we have discovered the first purely robotic lifeform to be documented thus far! While walking through the wetlands, we must have triggered some sort of alarm, as these mysterious beings assembled themselves and quickly descended upon us. I will describe their properties below. I have assigned their common name as “Brass Contraption.”

* They are capable of flinging spiked artillery with incredible force. Tharson swiftly dodged the Contraption’s fire, yet it still punched through a nearby tree with little effort, toppling it.

* They appear to sense their surroundings targets through echo-location. They constantly emit a low ringing sound from a bell-like device at their core, and their accuracy was substantially affected after Tharson landed a clean blow on the bell, denting the device.

* They are constructed of an alien alloy currently under investigation by our friends on the science team. It resembles Earthen brass, yet appears to have durability and toughness more resembling steel.

* How the Contraptions function is still a mystery, as they possess no circuitry or onboard computers. Whatever force animates them must be unique to this planet, as no level of technology this advanced has ever been documented.</pre>
{{< /rawhtml >}}

