---
title: Voidling
image: /img/Voidling_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Voidling_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">We almost got rid of it.

It was too massive, with too much weight.  It took up too much room, and was too dangerous.  We tried to massage it into its surroundings - we felt we had captured its essence well enough.  But its words beat into our minds and we weren't allowed to realize that it had been keeping us here.

We had begun spending all our days staring at it until it became formless to us.  Whispers of meaning with no words travelled through our ears and strung us up like a necklace of fools.  Totally mesmerized we sat around it like a campfire as it cultivated each of us with its cold embrace.  One by one we were scanned, and we were made to present all we had with us:  Feathers from local species, masks from strange spirits, abandoned metal scraps and rusty old equipment.  It examined each individually, its iris flexing mechanically as it traced the silhouette of every artifact.

For a moment I snapped out of it.  I felt fear.  I wanted to scream out, to raise my weapon, but my body was wholly resistant.  Suddenly, the others all turned their heads to face me from across the circle, their eyes wide and faces expressionless.  I felt a sharp pain in my chest and my neck stiffened.  After its final scan it vibrated out of existance, its atoms disseminating into the air.  We were left frozen husks around a campfire long since turned to ash.

We almost got rid of it.</pre>
{{< /rawhtml >}}

