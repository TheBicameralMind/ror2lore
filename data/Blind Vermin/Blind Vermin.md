---
title: Blind Vermin
image: /img/Blind_Vermin_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Blind_Vermin_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
 $ Combing for relevant data... done.
Complete!

</span>This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

 Tharson and I had been tracking a group of small, rodent-like fauna for some time. They are hostile on sight, but we hope to follow them back to their dens and observe their behavior when not hunting. I have assigned their common name as "Radkari Blind Vermin".

* As their name suggests, the Vermin are blind, relying primarily on their acute senses of smell and hearing to navigate their environment.

* They leave behind trails of body waste as they scurry to and fro, and other Vermin follow these trails, much like Earth ants' pheromones. Could these creatures be descended from insects?

* The Vermin are weak individually, and thus live in communal dens, typically buried in hillsides or foxholes.

* Their terrible odor cannot be understated. Even through our suits' filters, we could still smell a foul odor, reminiscent of rotting flesh and excrement. I will not have Tharson disable his filters, despite my curiosity.</pre>
{{< /rawhtml >}}

