---
title: Shipping Request Form
image: /img/Shipping_Request_Form.png
tags:
- Utility Items
---

{{< figure src="/img/Shipping_Request_Form.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">This is an automated message. If you do not have reason to receive this message, please contact UES Customer Support.</span>

Dear {customerFirstName} {customerLastName},

We regret to inform you that your delivery of {package} has been canceled. We at UES understand the inconvenience this has caused you, and have decided to reimburse you with a UES Shipping Request Form. When redeemed, we at UES will waive all shipping fees and upgrade your shipment to expedited shipping at no extra cost, allowing you to receive your package as quickly and smoothly as possible. We once again apologize for the inconvenience.

The reason your package was canceled is due to the shipping vessel it was on, UES Contact Light, has mysteriously disappeared in uncharted territories and we have not received communications since. If you or a loved one were aboard the Contact Light, please contact your local authorities if you have not done so already. Please let it be known that all of our efforts are going towards locating the Contact Light and her crew and returning them safely home with UES-ensured comfort and security.

We thank you for your continued support,

{SysAdmin}

<span class="mono">If you would like to join our reading list, reply with subject headline: UES.RL_JOIN</span></pre>
{{< /rawhtml >}}

