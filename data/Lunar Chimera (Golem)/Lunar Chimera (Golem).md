---
title: Lunar Chimera (Golem)
image: /img/Lunar_Chimera_(Golem)_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Lunar_Chimera_(Golem)_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">Your completions of my designs are uneasy, brother. Let me show you their purpose.

We begin with stone, silver, and fire. Take note of the ratios.

High speed. Speed is war. 

Twin cannons – with twin exhausts. We stream their exhausts, for volatility.

Quadrupeds are easier, and more stable. We maintain that.

And that, dear brother, is it. No dance. No song. These guardians do not need soul.</pre>
{{< /rawhtml >}}

