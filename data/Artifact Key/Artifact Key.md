---
title: Artifact Key
image: /img/Artifact_Key.png
tags:
- WorldUnique Items
---

{{< figure src="/img/Artifact_Key.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The key is incredibly simple. A stone crescent, with little discernable detail to the naked eye. But if one looks closer, with a sight beyond sight, they may notice the patterns inscribed on the key’s being. The patterns tell many things:

The key’s function: to unlock a lost trove of abandoned treasure.

The key’s creators: two brothers, one composed of ambition and the other compassion.

The key’s hopes: infused into the key, deep within its core, is a memory of better times, and the hope that one day, perhaps things could be better again.

The key’s reality: the bitter realization that such a reality exists only in the past.</pre>
{{< /rawhtml >}}

