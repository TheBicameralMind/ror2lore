---
title: N'kuhana's Opinion
image: /img/N'kuhana's_Opinion.png
tags:
- Damage Items
---

{{< figure src="/img/N'kuhana's_Opinion.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Let us begin.

What is your oath?

Not very long ago, health and time were in perfect synergy. Pain and love. Death and memories. The great equalizer. She would always come for us. 

But! Disparity now plagues our generations - and our children's. Not many pass, but so many are born. So many doomed souls, saved. Our hubris as a race continues to grow - unchecked. Unrestrained. 

Now there is no volume in our world for death, and she cannot visit us. But is being alive only an offset from death? Without contrast, won't we all be dead in life? The standard has shifted, and now we are all so very close to never living again.

We must make volume for her name - to restore the balance. Let us consume the Concepts, so we may begin to drain the bloated cistern that is our world. As disciples, we will spread her words and opinions. As pupils, we will sow death. And should we be lucky... be granted an audience by Her.

Weshan!"

-The N'tormat, Chapter I, Stanza I, Verse II</pre>
{{< /rawhtml >}}

