---
title: Shuriken
image: /img/Shuriken.png
tags:
- Damage Items
---

{{< figure src="/img/Shuriken.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Aluminum Throwing Star
Tracking Number: 77******
Estimated Delivery: 05/15/2060
Shipping Method: Priority
Shipping Address: Sensei Dareth's Mojo Dojo, Earth
Shipping Details:

For the last time, shouting "hiyahh!" when throwing a shuriken will NOT improve your results. It just gives your position away... You know, the exact OPPOSITE of what a ninja wants.

However, you can say "hiyahh!" in your head. (I do too.)</pre>
{{< /rawhtml >}}

