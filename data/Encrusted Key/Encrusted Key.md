---
title: Encrusted Key
image: /img/Encrusted_Key.png
tags:
- Utility Items
---

{{< figure src="/img/Encrusted_Key.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

"I have this key. I found it one day when I was taking a walk along the beach... It was washed up on the shore, all covered in crust and sand. I dunno why, but I took it with me... I figured it would be neat to try and figure out what this key went to. It'd be like a treasure hunt, y'know? Thing is, no matter who I went to, I never found anything. Any lock that seemed to match, the key wouldn't go in all the way. I was starting to get frustrated. I... I'm not too religious, but for some reason, I felt it was my destiny to use this old, beat-up key and find its match. M-Maybe it would change my life. I dunno. And, when I heard about this mission, for some reason, my heart began to race. I've heard all about the Contact Light's disappearance, everyone has. But... I don't know why, but I felt, in my heart of hearts... Whatever this key went to was where the Contact Light was. And it was my destiny to come along."</pre>
{{< /rawhtml >}}

