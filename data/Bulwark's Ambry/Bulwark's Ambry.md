---
title: Bulwark's Ambry
image: /img/Bulwark's_Ambry.png
tags:
- Environments
---

{{< figure src="/img/Bulwark's_Ambry.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">I’ve begun to understand our universe. The compounds that drive reality here. I’ve prepared a demonstration - come look, brother!

Now, look here. It’s got presence and weight. Round, with no features. No orientation. It is mass. Almost everything here is mass. Mass is boring, but required.

We can take mass - and shape it to form. Features, angles, and intent. Look at the planarity of the surfaces. This is design. I love design.

Now, a volatile one - a lifeline. Blood. Sharp, yet radiant. Blood is heat. This is my blood, but there is plenty of blood in this universe. 

Now be gentle. Look closely here. Do you see it? Almost invisible, a thin film. Frail. It’s not in this reality, but an adjacent one. I’ve heard you discuss this - something you innately understood. What you describe as soul. Don’t you love soul?

Do you understand? It’s your turn, brother! I’ve simply found the compounds. It is your hands that must craft value from them.</pre>
{{< /rawhtml >}}

