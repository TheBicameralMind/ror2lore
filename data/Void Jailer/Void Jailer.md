---
title: Void Jailer
image: /img/Void_Jailer_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Void_Jailer_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">Deep, deeper, yet deeper.

In this world, there's always a bigger fish. And appropriately, a bigger space for it to inhabit.

The only way to find such places is to go deeper.

Deeper into curiosity. Deeper into hubris. Deeper into the depths of creation.

And what you may find are the ones who are able to thrive in the depths, those murky dimensions and stilled spaces.

Those who lurk in the calm, dark abyss at the bottom of everything.

And once you find them, it will only be too late. As you struggle against the void, you may find that every path you take just brings you deeper, deeper, yet deeper.</pre>
{{< /rawhtml >}}

