---
title: Queen's Gland
image: /img/Queen's_Gland.png
tags:
- Utility Items
---

{{< figure src="/img/Queen's_Gland.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Yeah, uh, Kaitlin? Let me know if you copy."

"I copy. What's up?"

"I've made friends? I don't know. "

"What? "

"The giant bug we killed earlier - well, the corpse seemed to attract a bunch of nasties. I was on my way to clean them up when I realized they were... waiting? Confused, maybe. Anyways, I was walking up to them to get rid of em, and strangely enough they seemed passive. The same bugs that gave me that concussion yesterday, and-"

"What?! Are you okay? "

"Huh? Yeah, I'm fine. Anyways, I felt bad killin' them since they seemed all... inert and such. So I just kinda left and they've been following me ever since. Real weird. Anyways, I guess I've made some friends. I wonder what they eat... do they eat? Since they don't have any mouths that I-"

"Rich, stop! Please don't bring them back to camp! "

"Oh, well uh...I could - Junior, stop! Leave your brother alone! "</pre>
{{< /rawhtml >}}

