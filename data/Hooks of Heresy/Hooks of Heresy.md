---
title: Hooks of Heresy
image: /img/Hooks_of_Heresy.png
tags:
- Damage Items
---

{{< figure src="/img/Hooks_of_Heresy.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“…her arms were warped into terrible blades, so she may no longer find joy in study or tooling…”

-The Evisceration of Kur-skan the Heretic, IV</pre>
{{< /rawhtml >}}

