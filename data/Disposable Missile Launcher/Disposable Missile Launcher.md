---
title: Disposable Missile Launcher
image: /img/Disposable_Missile_Launcher.png
tags:
- Equipment
---

{{< figure src="/img/Disposable_Missile_Launcher.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Consumers were shocked when a beloved game company released its latest cardboard assembly kit: a missile launcher.”

- The Saturn Reporter</pre>
{{< /rawhtml >}}

