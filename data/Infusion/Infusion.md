---
title: Infusion
image: /img/Infusion.png
tags:
- Utility Items
---

{{< figure src="/img/Infusion.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Infusion
Tracking Number: 768******
Estimated Delivery: 02/8/2056
Shipping Method: Priority/Biological
Shipping Address: Rage Valley, Fleet Hanger, Mars

Contains samples from bears, leeches, tigers, elephants, elephant sharks, sharks, bull sharks, ants, and anteaters. Simply hook up to a dialysis machine along with the necessary equipment and swap out your blood for genetically superior ones! 

You can add whatever blood sample you want, as far as I know. Just make sure you take the pills that allow the body to accept the new blood, or your body will reject the cross-species infusion. Remember that sampling from other animals is a great basis for experimentation!</pre>
{{< /rawhtml >}}

