---
title: Tentabauble
image: /img/Tentabauble.png
tags:
- Utility Items
---

{{< figure src="/img/Tentabauble.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Mobile Terrarium + Chronobauble
Tracking Number: 99******
Estimated Delivery: 03/03/2076
Shipping Method: Biological
Shipping Address: 9042 Pvt. Drive, Yustik Plaza, Mercury
Shipping Details:

Hey, me in the past. This'll sound really corny, but I've been really into gardening lately. Now, before you say anything, you gotta think about the situation I'm in. I mean, with all the stuff going on, anyone would go nuts if they had to deal with - oops. No spoilers, my bad.

Anyway, remember that chronobauble? The one we found on Mercury? Turns out, it has a "fast-forward" feature. I, um, tried to use it to help some hybrids I was growing mature faster. Botany stuff, you'll get it later - well, I may have made it go a bit too fast. This thing must've gone through a few millennia by the time I turned the dial back to 0. I'm pretty sure this is like, an entirely new genus of plant? Maybe even an entirely separate species. Note to self: remember to never turn the dial up to 11. You WILL create an abomination of evolution.

Anyways, same drill as last time - shipping this to myself in 10 years. 10 years passes quickly, especially if it's contained within a locally accelerated temporal field. Yeah, it'll go by in the blink of an eye - alongside a couple millenia! Maybe I should take up scrapbooking next.</pre>
{{< /rawhtml >}}

