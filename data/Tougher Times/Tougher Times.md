---
title: Tougher Times
image: /img/Tougher_Times.png
tags:
- Utility Items
---

{{< figure src="/img/Tougher_Times.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM ROOM 4211B OF UES [Redacted] <span>--</span>//</span>

"Why'd you sign up?"

Wyatt's voice was bored. The mission to the Contact Light's last known location was taking longer than he expected.

Malik was on the floor, cleaning her rifle. "To get paid, mostly."

"And that's it?"

She grunted in affirmation. She continued to wipe down the weapon.

"I'm here to find something." He continued. "I shipped it a few years back, but I don't think it made it before the train went down. I'm guessing it's still in the Contact Light. So I'm gonna get it back."

She began to tighten the last remaining bolts. Her routine near completion, she moved her gaze to Wyatt. "Seriously?"

"Yep!"

Malik raised her brow. "There were over 7 million security chests in the Contact Light. The chance of you finding a specific one is impossible." She began to unfold her legs as she propped her rifle in the corner. "And according to the distress beacon, the Contact Light was destroyed in orbit. Your shipment is probably floating around in space. It's all gone, Wyatt."

He grinned. "I think I'm gonna find it."

"You're terrible."</pre>
{{< /rawhtml >}}

