---
title: Delicate Watch
image: /img/Delicate_Watch.png
tags:
- Damage Items
---

{{< figure src="/img/Delicate_Watch.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The wind blows over the plains. Two soldiers trudge along on their routine patrol, filling their boredom with casual conversation. "Hm. Hey, Shelly, have I ever showed you my new Patex?" Quinton mused. Shelly shot him a quizzical look. "Tell me you didn't bring a $75,000 watch with you on a dangerous expedition into unknown territories..."

Whirling around to face his partner, Quinton gave a hearty laugh. "Why, yes I did!" Rolling up his sleeve, a glint of gold revealed his collector's watch. The metal surface gleamed proudly, reflecting a ray of sunlight into the eyes of a hidden Lemurian. "What's the point of going through trials and tribulations in the middle of nowhere if I can't STYLE all over my fellow soldiers!?" Quinton laughed, pounding his fist to his chest. "I'm going to rub it in your face SO HARD when that thing inevitably breaks," Shelly chuckled. Quinton scoffed. "Oh, please. We've been along this route countless times, and nothing's happened. We're lucky to be stationed on a quiet sector of this hellhole, and I doubt our luck will run out any time soon."

As if on cue, the aggrivated Lemurian, annoyed by the glare, leapt from the bushes and shot a fireball. "Woah!" Shelly shouted, raising her gun and killing the beast. "Hah... So much about a quiet, sector, huh?" Shelly turned to her partner, who was doubled over on the ground. Shelly's face blanched.

"Oh no... Were you hit? We need to get you to a medic, fast...!"

"No." Quinton's voice was small and full of grief. "I'm perfectly fine, but..." Quinton looked up, revealing his gleaming Patex, having taken the fireball dead-on, had been reduced to a mangled mess of twisted metal and smoking polish. "L-Look what that BEAST did to my precious watch!"

For a moment, all was quiet on the plains. Then, the silence was yet again broken by Shelly's laughter and Quinton's desperate pleading.</pre>
{{< /rawhtml >}}

