---
title: Milky Chrysalis
image: /img/Milky_Chrysalis.png
tags:
- Equipment
---

{{< figure src="/img/Milky_Chrysalis.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Milky Chrysalis
Tracking Number: 393*****
Estimated Delivery: 11/27/2056
Shipping Method: High Priority/Biological
Shipping Address: Diptera Research Center, Io
Shipping Details:

Deep within the plague wastes there is an alcove free from disease. It is home to a group of insects that have NOT acclimated to their environment. They survive not through resistance… but through transformation. 

I’ve witnessed the creatures undergoing infinite states of metamorphosis. Each change that happens to them is reflected in the surrounding land. This constantly molds the area into something less toxic.

I can’t imagine that they don’t have an upper limit - a final form. What adjustments would that final form bring? Are there any restrictions to what can be modified? I’m sending this sample to you to study. Please be careful with it, as it’s frozen in a cryo cell mid-transformation.</pre>
{{< /rawhtml >}}

