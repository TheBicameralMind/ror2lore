---
title: Hermit Crab
image: /img/Hermit_Crab_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Hermit_Crab_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

We suddenly came under fire when Tharson was smacked in the head by a glob of mucus and debris – Tharson is OK from this encounter. Tracing the trajectory, we followed the projectiles’ path to its source, a large crustacean-like creature.

I will describe its properties below. I have assigned its common name as “Forhamian Hermit Crab.”

* The Crab disguises itself as a stalagmite – careful observation can reveal the Crab’s true identity.

* The Crabs dislike up-close contact with prey, fleeing upon approach. It attacks from exclusively long-range by expelling balls of mucus and body waste with incredible force.

* The Crabs don’t seem to have eyes – often stumbling around and bumping into walls. How they can locate targets from a long-range with decent accuracy is currently unknown.

* The Crabs seem to have “favorite shells.” I observed several specimens clearly too large for their shells, only ditching their shells once they are damaged and forced to find a new one. Some Crabs also seem to have favorite materials that they prefer.</pre>
{{< /rawhtml >}}

