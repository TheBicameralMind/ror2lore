---
title: Brainstalks
image: /img/Brainstalks.png
tags:
- Utility Items
---

{{< figure src="/img/Brainstalks.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order:  Biopsy Samples of Cairne Jefferson
Tracking Number: 051*****
Estimated Delivery: 11/11/2056
Shipping Method: High Priority/Biological
Shipping Address: Saura Cosmo, Beacon Post, ???
Shipping Details:

Contained in this shipment should be a variety of biopsy samples from our late Mr. Jefferson. As you know, he was an extraordinary man in almost any manner. He was athletic, brilliant, kind, funny, and an all-around great human specimen. 

He donated his body to science, and as we began the operation we found a most terrifying discovery.

A quick visual examination of the subject’s brain shows a very… particular oddity. It seems to be housing a variety of… glowing brain “stalks”, similar to tubeworms. Trying to biopsy the stalks is impossible - they seem to disintegrate into dust the moment we remove it from the brain. We cannot explain this oddity at all. As such, we have included the entire brain in this shipment.

Please let us know if you find any explanation.</pre>
{{< /rawhtml >}}

