---
title: Molotov (6-Pack)
image: /img/Molotov_(6-Pack).png
tags:
- Equipment
---

{{< figure src="/img/Molotov_(6-Pack).png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Ethanol Bottle (32 oz.), 6 Pack
Tracking Number: 81******
Estimated Delivery: 05/8/2058
Shipping Method: Priority
Shipping Address: Teromere Manor, Privet Road, Mars
Shipping Details: Let our friends inside know that we're coming over for drinks on the 16th of June. It'll be a hell of a party, they should probably hit the road before things get too out of hand.</pre>
{{< /rawhtml >}}

