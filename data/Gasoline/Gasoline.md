---
title: Gasoline
image: /img/Gasoline.png
tags:
- Damage Items
---

{{< figure src="/img/Gasoline.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Audio transcription complete from signal echoes. Assigning generic tokens.</span>

[Fire crackling]

MAN 1: D-do you think they're gonna come for us?

MAN 2: They'll try. It's going to be a very long while.

MAN 1: What? Why?

MAN 2: A long while. Even if they know where to look we'd be months out from the nearest port. And that's if they even have any ships as fast as ours – FTL ships are very rare nowadays.

MAN 1: Months...?! And what do you mean if they know where? What about the other ships on our shipping routes?

MAN 2: We weren't on the route.

[Fire pops]

MAN 1: What?!

MAN 2: We should've been halfway to Procyon by the time we crashed... but we weren’t. The ship never announced it was slowing down either, so that’ll make triangulating our positions even harder.

MAN 1: I-I don't get it. Who would take a UES train off course? That's completely insane!

MAN 2: I don’t know – only the Captain does. There’s no reason to slow down in this star system - there's not even supposed to be a habitable planet out here.

[Sizzling]

MAN 2: This looks cooked to me. Can't vouch for how it'll taste - but we have to eat.

MAN 1: I... I can't even think right now. I’m not hungry.

MAN 2: Eat. We've got a lot of traveling to do tomorrow and we'll need to keep our strength.

MAN 1: Sure. Okay. Okay. Um… do you think it's poisonous?

MAN 2: Eat.

<span class="mono">End of requested transcript. </span></pre>
{{< /rawhtml >}}

