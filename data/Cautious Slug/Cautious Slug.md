---
title: Cautious Slug
image: /img/Cautious_Slug.png
tags:
- Healing Items
---

{{< figure src="/img/Cautious_Slug.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“One of the most crucial things to remember about having an animal companion is that your relationship is built on trust. Though humans are perceptive, we tend to be blinded by what we see – and we ignore the dangers that lurk beyond our vision. Animals, and other xenobiology, can sense things we can’t. 

So always remember: if your companion warns you of danger in the dark, you had better well listen.”

- Caring for your Pets, Volume III</pre>
{{< /rawhtml >}}

