---
title: Sticky Bomb
image: /img/Sticky_Bomb.png
tags:
- Damage Items
---

{{< figure src="/img/Sticky_Bomb.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">In the blaring heat, a silhouette could be seen, hiding in the cool shade of a nearby cave. She was waiting for the sandstorm to pass.

She heard a noise and turned around, recognizing another silhouette. It bounded into the cave with excitement. As the figure got closer, she alarmingly noticed the cargo.

"Woah, what the hell?! Why are you holding like, fifty bombs? Get away from me! "

"What! We need these – this planet is trying to kill us! "

"You’re a walking bomb. Those are so unstable. Get away! Where did you even find those things? "

"I found an industrial 3D printer nearby, and it was assigned to the blueprints of these bombs. Awesome, right? Unlimited bombs!"

"…What materials?"

"Huh?"

"What materials did you put in? We barely had anything."

"I just put in everything."</pre>
{{< /rawhtml >}}

