---
title: Abandoned Aqueduct
image: /img/Abandoned_Aqueduct.png
tags:
- Environments
---

{{< figure src="/img/Abandoned_Aqueduct.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
Outputting local audio transcriptions...
</span>

<span class="mono">Day 0</span>
Nightmare. I find myself in the middle of a desert - with no other survivors in sight. I scavenged for resources, but soon giant insects began to crawl out of the earth and take aggressive stances. Once they gathered their numbers - and presumably their confidence - they charged me. 

In avoiding their assault, I slipped and fell backwards into a waterfall of mysterious liquid. I tumbled within as it rushed me down, deep into an underground cavern. I crawled out, and thankfully the insects have stopped following me. I may have broken my leg - but thankfully CHIT(?) I have a healing drone to administer aid. It will take a few days to get my strength back. 

<span class="mono">Day 1</span>
I am grateful that I repaired this drone before my untimely tumble - I would assuredly be dead without it. My vision is blurry - I fear I have also suffered a CHIT concussion. My leg has begun to feel better. I will have to continue to wait. I grow hungry.

<span class="mono">Day 2</span>
My healing drone has stopped giving me aid. It hovers far away now - and moves back as I move forward. I've been feeling nauseous, and have been feeling the urge to CHIT CHIT vomit. I occasionally spit out black liquid - I fear something is gravely wrong.

<span class="mono">Day 3</span>
My CHIT CHIT drone has left.  I have CHIT lost my vision. My mouth and eyes fill with foul dark liquid. I am alone.

<span class="mono">Day 9</span>
CHIT CHIT CHIT CHIT CHIT CHIT CHIT CHIT CHIT I-I neED CHIT CHIT CHIT chit chit chit CHIT
[Report detected!]

<span class="mono">Local audio transcriptions complete.</span></pre>
{{< /rawhtml >}}

