---
title: Frost Relic
image: /img/Frost_Relic.png
tags:
- Damage Items
---

{{< figure src="/img/Frost_Relic.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Sven Volz discovered the specimen that holds the record of Galaxy’s Largest Snowflake: a whopping 0.5 meters in diameter, with an average temperature of -52 degrees. However – much to the despair of novelty snowflake enthusiasts everywhere – this gargantuan snowflake was lost in transit aboard the UES Contact Light, which disappeared in uncharted territory. One can only hope that it is preserved and frosty as ever, where ever it may be.”
- Academy Galactic Records 2056</pre>
{{< /rawhtml >}}

