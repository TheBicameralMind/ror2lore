---
title: Resonance Disc
image: /img/Resonance_Disc.png
tags:
- Damage Items
---

{{< figure src="/img/Resonance_Disc.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Turbine Configuration Wizard Disc
Tracking Number: 64***********
Estimated Delivery: 08/23/2056
Shipping Method:  High Priority
Shipping Address: 73421, Calorum St., Saturn
Shipping Details: 

Oops, so sorry! I forgot to include the installation wizard with your new turbine. Thanks so much for letting me know. Who knows what could have happened if you didn’t configure the turbine prior to installation? Always make sure to run the diagnostics on this disc every few weeks – it’ll help with the turbine’s longevity.</pre>
{{< /rawhtml >}}

