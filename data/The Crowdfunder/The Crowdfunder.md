---
title: The Crowdfunder
image: /img/The_Crowdfunder.png
tags:
- Equipment
---

{{< figure src="/img/The_Crowdfunder.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: The Crowdfunder
Tracking Number: 783*****
Estimated Delivery: 05/09/2056
Shipping Method: Standard
Shipping Address: 206 29th Ave, High Chariot, Mercury 
Shipping Details:

The wealthy overlords on Primas V went to great measures to break the will of the lower class. They kept the threat of injury and death constant, which helped to keep their servants in check, but this was only half of their method. 

To further highlight the vast differences in status, their extravagant weapons were fueled by currency - they were literally throwing money at them.

I don’t have to tell you how that turned out for them. It’s why my client is in hiding now and has to pawn these things off. He thanks you for your patronage in these trying times.</pre>
{{< /rawhtml >}}

