---
title: Focus Crystal
image: /img/Focus_Crystal.png
tags:
- Damage Items
---

{{< figure src="/img/Focus_Crystal.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Condensed Matter Crystal [50kg, 10cm by 10cm by 10cm]
Tracking Number: 12***********
Estimated Delivery: 05/23/2056
Shipping Method:  Standard
Shipping Address: Geofferson Principality, Highward, Titan
Shipping Details: 

Hope they don’t up the price for this thing. I haven’t had troubles with UES before, but I’ve never had to ship a crystal this far before. Let me know if the package is marked as “heavy” when you get it.</pre>
{{< /rawhtml >}}

