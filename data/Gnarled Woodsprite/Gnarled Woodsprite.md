---
title: Gnarled Woodsprite
image: /img/Gnarled_Woodsprite.png
tags:
- Equipment
---

{{< figure src="/img/Gnarled_Woodsprite.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Gnarled Woodsprite
Tracking Number: 447*****
Estimated Delivery: 01/16/2056
Shipping Method: High Priority/Biological
Shipping Address: Happy Hope Children's Hospital, Waleton, Earth
Shipping Details:

Dr. Coleas's studies on the benefits of sprite-assisted care have paved the way for hospitals across Earth. These little guys take every opportunity to radiate amongst the injured and sick. 

We've seen significant decreases in the recovery times of almost all patients. While effective, I'm not certain if this aura effect is transmitting anything at all. It might all be placebo; people could just be reacting to the spectacle of a cute lifeform putting on a light show in front of them.

Either way, it's good to have them around, and I think they enjoy being around us too. I'm lucky enough to get to keep one in my home... for study purposes, of course. It may seem a little crass, but shipping these little guys in from off world does not seem to impact their attitude in the slightest.</pre>
{{< /rawhtml >}}

