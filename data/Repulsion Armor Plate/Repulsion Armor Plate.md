---
title: Repulsion Armor Plate
image: /img/Repulsion_Armor_Plate.png
tags:
- Utility Items
---

{{< figure src="/img/Repulsion_Armor_Plate.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Repulsion Armor Replacement Piece
Tracking Number: 09******
Estimated Delivery: 08/15/2056
Shipping Method:  Standard
Shipping Address: System Police Station 13, Port of Marv, Ganymede
Shipping Details:

Luckily no one was hurt during the shootout. Just a few rough characters at the bar by the docks. Nothing we couldn’t handle. Jaime took a shot to his shoulder but his armor took all the impact. We’ll need to order him a replacement part before he can go back out in the field.

The segmented design is nice because I don’t have to shell out the cash for a whole new set. Frankly, the station’s coffers have seen better days. The next time a rookie damages their equipment they might be looking at a desk job for a while.</pre>
{{< /rawhtml >}}

