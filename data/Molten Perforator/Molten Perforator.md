---
title: Molten Perforator
image: /img/Molten_Perforator.png
tags:
- Damage Items
---

{{< figure src="/img/Molten_Perforator.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>

Subject: Molten Perforator
Technician: Dema "Dembones" Brown
Table Spec: Heatsink UB-2
Notes:

&gt; Using UB-2 due to temperatures above safe levels.
&gt; Removing molten enamel and placing aside for substance analysis. It’s solid, yet swimming.
&gt; Upon structural investigation, found cavities and internal chambers
&gt; Reduce lab temperatures by 10 degrees
&gt; Heat generating veins present - fire is being supplied to the tooth?
&gt; Removed my lab coat, very hot
&gt; Heat generation is still occurring in the severed object
&gt; Put some more ice in my drink WOW it's hot
&gt; Timestamping for break</pre>
{{< /rawhtml >}}

