---
title: Void Devastator
image: /img/Void_Devastator_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Void_Devastator_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">"There's old stories of an deity who commanded the waves. Not just the kind that lap at the shore on a sunny day, no, I'm talking the kind that brings cliffsides down. The kind of current that could effortlessly bat ten-ton cars aside, uproot skyscrapers, and drown entire cities. There's more, too. They say he also had the power to create earthquakes. Can you imagine that kind of power? Natural disasters are still a plague to this day, even in the advent of space travel. Could you imagine what it would be like to be in the presence of someone who was the master of such power? Who could call down a tidal wave, and smother your life away, with a snap of their fingers?

Such events undoubtedly inspired stories of gods who cast these events on their followers. Maybe out of spite, maybe out of some sense of justice. All I know is, if I were to look upon a being of such power... I'd cut my losses and run."

Memoir of Sean Locke, Mythology Professor</pre>
{{< /rawhtml >}}

