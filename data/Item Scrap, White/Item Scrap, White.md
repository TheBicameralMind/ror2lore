---
title: Item Scrap, White
image: /img/Item_Scrap,_White.png
tags:
- WorldUnique Items
---

{{< figure src="/img/Item_Scrap,_White.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“While officially licensed materials are always recommended, hobbyists are known to take matters into their own hands when in a pinch. Don’t have any materials up to par? Make your own! Recycling old parts can be a fun way to create materials for your projects.”

- The Ultimate Hobby Guide: Kitbashing and 3D Printing</pre>
{{< /rawhtml >}}

