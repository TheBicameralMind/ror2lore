---
title: Mithrix
image: /img/Mithrix_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Mithrix_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">They knew to stay away from the wells - but they were just kids. And they just wanted to know.

They threw in dirt and stone and kifruit and starseed, watching as they collapsed. Thorp! The gravity wells would swallow up anything. Glass and mud and silver. Wind. Heat. Pulsar radiation.

And when his brother wasn’t watching, Mithrix would throw in worms. Thorp! But only when he wasn’t watching. You see, his brother loved worms. It would make him sad. But Mithrix didn’t care much about worms. He was just curious.

But one time, his brother was watching. And his brother loved worms.</pre>
{{< /rawhtml >}}

