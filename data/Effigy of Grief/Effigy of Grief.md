---
title: Effigy of Grief
image: /img/Effigy_of_Grief.png
tags:
- Equipment
---

{{< figure src="/img/Effigy_of_Grief.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“This relic tells a story... But it is not a fairy tale. It’s a tragedy. A story of betrayal, regret, and sorrow. A story of two.”

“Uh… okay…? How the hell do you know that?”</pre>
{{< /rawhtml >}}

