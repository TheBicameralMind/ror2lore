---
title: Plasma Shrimp
image: /img/Plasma_Shrimp.png
tags:
- Damage Items
---

{{< figure src="/img/Plasma_Shrimp.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"I believe there is a benefit to attacking from behind an impenetrable defense. Some may call it cheating, they may call you a coward, even... I call them targets. History is written by the winners, after all."

- Lost Journal, recovered from Petrichor V</pre>
{{< /rawhtml >}}

