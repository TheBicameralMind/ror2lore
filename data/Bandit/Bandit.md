---
title: Bandit
image: /img/Bandit.png
tags:
- Survivors
---

{{< figure src="/img/Bandit.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">A few drinks here, a few laughs there. A few more drinks. A sleight of hand – and bang, keycard access to the loading bay of the Safe Travels. The camo suit can handle the rest.

A secret mission means low profile – and low security. Besides, everyone is wearing environment suits before we leave orbit. How would they know?

I mean, it’s the goddamn <i>Contact Light</i>. Think of how much those higher security chests could flip for! And some of the militech on that ship? Howdy hey. That’s probably what some of the other boys were thinking, sneaking onto that ship in the first place.

My coat? 

I’m a thief - not a plumber. Of course I’m bringing my coat.</pre>
{{< /rawhtml >}}

