---
title: Hunter's Harpoon
image: /img/Hunter's_Harpoon.png
tags:
- Utility Items
---

{{< figure src="/img/Hunter's_Harpoon.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Harpoon
Tracking Number: 01******
Estimated Delivery: 07/21/2058
Shipping Method: Priority
Shipping Address: Saint Mary's Port, Keelhaul Key
Shipping Details:
They say that you can spear two catches with one throw of this ol' lassie. And hey, if you can't, if you're fast enough, nobody will be able to tell the difference! Wahahaha!</pre>
{{< /rawhtml >}}

