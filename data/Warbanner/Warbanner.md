---
title: Warbanner
image: /img/Warbanner.png
tags:
- Utility Items
---

{{< figure src="/img/Warbanner.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Heavy losses were expected – after all, the invaders were as cunning as they were deadly. 

The dust had at last settled. The Golems had returned to the earth, and the Lemurians had returned to their tunnels. It was a long and grueling battle, but they had won. The invaders had been purged.

The only proof of their last stand was a single banner, fluttering in the wind, flying over a mountain of corpses.</pre>
{{< /rawhtml >}}

