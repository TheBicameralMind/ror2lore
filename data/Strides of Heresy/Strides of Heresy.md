---
title: Strides of Heresy
image: /img/Strides_of_Heresy.png
tags:
- Utility Items
---

{{< figure src="/img/Strides_of_Heresy.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Her legs were scattered to the two poles of the moon, twisted in a wicked position, in a field of obsidian thorns…"

-The Evisceration of Kur-skan the Heretic, V</pre>
{{< /rawhtml >}}

