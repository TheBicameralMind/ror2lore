---
title: Eccentric Vase
image: /img/Eccentric_Vase.png
tags:
- Equipment
---

{{< figure src="/img/Eccentric_Vase.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“One of Robins’ most loved acts was the pitcher skit. 

The skit would start off with Robins, playing the part of a flower keeper, getting ready to fill a vase with soil. He would pour, pour, and pour, but the vase wouldn’t seem to get any fuller – as if the soil was disappearing infinitely. 

Robins would inspect the vase, looking at it up and down, but there weren’t any holes in the thing. Finally, at the climax, Robins would hold it upside down over his head – upon which all of the soil poured in would land directly on his face.”

- Biography of Charles Robins</pre>
{{< /rawhtml >}}

