---
title: Railgunner
image: /img/Railgunner.png
tags:
- Survivors
---

{{< figure src="/img/Railgunner.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Snipes aren't real."

"...What?"

"That's the point of a snipe hunt:  to send a schmuck off into the woods looking for an animal that doesn't exist."

"...Except they totally exist."

"Haha, sure."

"No, really.  They're birds back on Earth.  Tiny, camouflaged birds.  Ever wonder why we call sharpshooters 'snipers'?  Because they're good enough to hit a snipe."

"No kidding?"

"Cross my heart and hope to die."

"Huh."

"You know, rails are a kind of bird too."

"Get out."</pre>
{{< /rawhtml >}}

