---
title: Happiest Mask
image: /img/Happiest_Mask.png
tags:
- Damage Items
---

{{< figure src="/img/Happiest_Mask.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM RALLYPOINT DELTA <span>--</span>//</span>

“Sir, the ghosts are back.”

The man sighed. After a routine expedition, one of the crew members – a simple soldier - had recovered an artifact thought to have been aboard the Contact Light – a simple mask, adorned with a painfully happy grin. 

“I’ll take care of it.” The man trudged down the hall towards the barracks. The Lemurians he had killed earlier that day walked down the hall by him, barely earning a second glance from the man. This had become so commonplace that most of the crew members in this block had grown accustomed to having a ghostly room-mate.

But enough was enough. Stepping through the ghost of an Imp, the man slammed the door open. The lights were off, and in the corner sat the soldier.

“Alright, we’ve had enough fun playing with the dead. Fork it over.”

No response. The man grunted and hoisted the soldier to his feet, giving him a few rough shakes. “Hey, can you hear me!? I said hand over the mask! I’m tired of waking up next to Beetles, so give it a rest already<span>--</span>”

The soldier’s limp body moved. Slowly, the soldier raised his finger – pointing directly at the man.

“What are you...?” With a sense of dread, the man turned and saw the Lemurians he had killed earlier step into the room. Their mouths began to glow with an otherworldly glow.

The man cursed under his breath as he loaded his shotgun. “This planet, I tell you...”</pre>
{{< /rawhtml >}}

