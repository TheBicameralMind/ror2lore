---
title: Roll of Pennies
image: /img/Roll_of_Pennies.png
tags:
- Utility Items
---

{{< figure src="/img/Roll_of_Pennies.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The sounds of gunfire rang in Johnson's ears. Biting down and tugging on the bandage covering his wounded leg, he hastily reloaded his firearm.

For a moment, before he fired on the Lemurians who had pinned his team into a corner and already claimed two of his friends, Johnson reflected on what brought him here.

Johnson had grown up in poverty, the oldest of three children. He had started working at a young age to support his sick mother and younger siblings, and all his life he bounced from one odd job to the next. Anything that could rake in the cash. Anything that would pay the bills.

"'An expedition into unknown territories, seeking volunteers,'" his recruiter read in between puffs of his cigar. "Lot of classified info, don't know too much myself... But it pays a lot. And I know you need the money. You in, Johnny-boy?" At the time, Johnson had thought to himself, 'it couldn't be that bad.'

Johnson returned to the battlefield, gunning down Lemurians in an effort to save his remaining friends. The wound on his leg ached, dying the bandage red with blood. 'It couldn't be that bad,' he had thought. "Yeah, right." Johnson scoffed. "At least I'm getting paid after this."</pre>
{{< /rawhtml >}}

