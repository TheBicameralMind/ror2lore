---
title: Pocket I.C.B.M.
image: /img/Pocket_I.C.B.M..png
tags:
- Damage Items
---

{{< figure src="/img/Pocket_I.C.B.M..png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"What's better than one missile...? MORE missiles. Easy, just fire a bunch of the things and reduce whatever you're fighting to a smoldering field of craters. Enemy nation? Missiles. Stupid bird taunting you? Missiles. Family argument going south? That's right - missiles! There's nothing a missile can't do, and what little the missile can't get done, a bunch of them can."

Editor's Note: Hey, um, maybe we should edit this out? Cluster bombs are still banned according to the geneva conventions... And urging people to nuke their family members PROBABLY won't go over well with the target audience.

The Autobiography of Jans Czar, Modern King of Weaponry, First Draft</pre>
{{< /rawhtml >}}

