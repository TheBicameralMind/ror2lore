---
title: Primordial Cube
image: /img/Primordial_Cube.png
tags:
- Equipment
---

{{< figure src="/img/Primordial_Cube.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Professors Mia Thatcher and Jared Malik report that the galaxy’s first singularity containment lattice – or SCL, for short – has finished tests and is confirmed to be perfectly safe. 

While this is a historic scientific breakthrough, multiple scientists have spoken out about the implications of such a discovery - and what it could mean for the future of the humanity.”

- The Saturn Reporter, Front Page Headline</pre>
{{< /rawhtml >}}

