---
title: Armor-Piercing Rounds
image: /img/Armor-Piercing_Rounds.png
tags:
- Damage Items
---

{{< figure src="/img/Armor-Piercing_Rounds.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Armor-Piercing Rounds, 50mm
Tracking Number: 15***********
Estimated Delivery: 3/07/2056
Shipping Method:  Standard
Shipping Address: Fort Margaret, Jonesworth System
Shipping Details: 

Alright, just to clarify, these rounds aren’t faulty. Heck, I’d say they’re better than the standard, but... that’s kind of the problem. I don’t know if it was a new shipment of materials, or a problem with the assembly line, but these rounds are supposed to pierce armor. Not pierce through the armor, five feet of reinforced concrete, a few warehouses, and an armored truck. 

Could you guys look into this so we don’t like, violate any Geneva Conventions or anything?</pre>
{{< /rawhtml >}}

