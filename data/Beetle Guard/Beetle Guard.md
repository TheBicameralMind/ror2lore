---
title: Beetle Guard
image: /img/Beetle_Guard_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Beetle_Guard_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

Amazing! We have witnessed our first ever eusocial creature outside of Earth. I observe from afar a giant cousin of the previously described Worker Beetles. 

I will describe its properties below. I have assigned their common name as ‘Beetle Guard’.

• Much greater in size than their smaller cousins, the Guard is the size of a rhinoceroses. The Guard has the same 5 eyes as the Worker's, with one of them protruding from the top of its head. This most likely allows the Guard to see while keeping its head down in a defensive position.

• The Guard appears higher in the social hierarchy than the Worker. I have witnessed Workers scatter when a Guard appeared.

• The Guard’s chitin plates extend to cover its entire body, with only joints revealing the thinner, darker exoskeleton below. 

• Tharson has reported that the Guard’s chitin is significantly sturdier than the Workers. We have consumed many resources to fell this magnificent creature, including our disposable missile launcher. Tharson’s battle with the Guard lasted for about an hour before it finally succumbed. Amazing! Tharson is OK from this encounter.</pre>
{{< /rawhtml >}}

