---
title: Shattering Justice
image: /img/Shattering_Justice.png
tags:
- Damage Items
---

{{< figure src="/img/Shattering_Justice.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Shattering Justice Owner’s Manual

Excerpt from section 2.13:
... and if the target material doesn’t cave under the immense weight just keep hitting it until it does. The patented technology inside the face of this tool doesn’t just use brute force to break down its mark, it also utilizes a formulated nano toxin, released through internal mechanisms, that builds up through subsequent contact. In other words, each hit makes it easier to break.

Please refer back to section 1.8 for information on proper handling and the included anti-toxin gloves...</pre>
{{< /rawhtml >}}

