---
title: Mercenary
image: /img/Mercenary.png
tags:
- Survivors
---

{{< figure src="/img/Mercenary.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The europian noble paced around in his chambers, nervously glancing at the antique clock adorning his wall. Tick, tock. Tick, tock.

The riots had been escalating and showed no signs of slowing down. Effigies burned, statues were torn down, and countless politicians turned their backs on the once-prestigious nobility of Europa. The people have had enough. Tick, tock. Tick, tock.

It was only a matter of time– either the noble would have to flee, or the mob would finally breach the palace. Even his bodyguards began to show signs of betrayal. Time, the noble thought, was something he did not have. The antique clock kept ticking, working upon the noble’s mind. Keeping him distracted - just long enough. Tick, tock. Tick, tock.

And as the antique clock kept ticking, the noble’s body hit the floor. The Mercenary sheathed his blade. His footsteps were unheard as he left the palace– masked by the rhythmic ticking of the clock. Tick, tock. Tick, tock.</pre>
{{< /rawhtml >}}

