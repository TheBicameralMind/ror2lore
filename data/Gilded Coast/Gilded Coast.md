---
title: Gilded Coast
image: /img/Gilded_Coast.png
tags:
- Environments
---

{{< figure src="/img/Gilded_Coast.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">A gilded cage – one of luxury and accommodation. The jailer took pity on the prisoner and fashioned an elaborate space for which the it may serve its eternal sentence. The jailer even allowed the prisoner company – smaller constructs, a few rowdy Lemurians. The scraps of creation. 

But a cage of gold - no matter how beautiful - is still a cage.</pre>
{{< /rawhtml >}}

