---
title: Tri-Tip Dagger
image: /img/Tri-Tip_Dagger.png
tags:
- Damage Items
---

{{< figure src="/img/Tri-Tip_Dagger.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>

The following is an audio transcript from the trial of D. Cooper, wanted for 5 counts of serial manslaughter.

“Mr. Cooper, do you recognize this item?”

“I do.”

“Is it true that you used this weapon on your victims?”

“Yes.”

“And, Mr. Cooper, is it true that... that you would use this dagger to expose the healed wounds of your victims? In an attempt to spill yet more innocent blood from them?”

“Oh yes. Was my favorite part.”

“Your favorite part, you say. Ladies and gentlemen of the jury, I think you all can see that Mr. Cooper is a truly deranged man - who took the lives of countless innocent people. However, if you are not fully convinced, I have one more piece of evidence to provide for the jury’s consideration. Your honor, if I may prepare an audio recording of Mr. Cooper?”

“Proceed.”

“Thank you, your honor. Now, please play back the recording.”

The remainder of this court transcript has been sealed.
Reason: i think im gonna be sick</pre>
{{< /rawhtml >}}

