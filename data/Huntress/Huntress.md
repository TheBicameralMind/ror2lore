---
title: Huntress
image: /img/Huntress.png
tags:
- Survivors
---

{{< figure src="/img/Huntress.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">She gracefully jumped off the roof of the building, plummeting to the alley below. She rolled into a handspring, vaulting through a broken window into the abandoned warehouse inside. Years of navigating the streets kept her body strong - and her mind sharp.

She held her breath as police ran past, looking for the jewel thief. She may have to maintain this position for a few hours - but she’s used to it.

Amiera was infamous in this city – and for her infamy, she racked up a significant bounty. Was it thirty-thousand credits now? Forty? She let out a small smile. That bounty was nothing compared to the treasure she held in her hands. This jewel could sell for a million credits - no, more. Tens of millions. This was one of the last few prismatic amethysts still left in the world. She clutched the stone closer to her chest.

The police seemed persistent this time. Amiera was fine with that - all she had to do was wait. She slowed down the pace of her breathing - she may be here for a few days.

At least, that’s what she thought - before a flurry of laser arrows pierced her brain, killing her instantly.

//<span>-----------------------------------</span>//

20,000 CREDITS HAVE BEEN DEPOSITED TO YOUR ACCOUNT

GOOD HUNTING</pre>
{{< /rawhtml >}}

