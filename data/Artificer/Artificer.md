---
title: Artificer
image: /img/Artificer.png
tags:
- Survivors
---

{{< figure src="/img/Artificer.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">With the intense belief that heaven is a planet, not a mystical place, the High Court search deep into space for the Promised Land. 

Intensely driven by both dogma and science, the High Court holds amazing technological marvels - with nearly all of them hidden deep within their halls. 

The House Beyond represents the most ambitious of the High Court's followers, spearheading Zone 5 deep space travel. With the advantage of their unique ENV Suit, the House Beyond have gone deeper in space than any other House in the High Court by an order of magnitude. The ENV Suit, worn by the Artificers of the High Court, is an engineering marvel - able to calibrate to the conditions of any environment. The technology behind the ENV Suit, like all other High Court technology, is still undisclosed.</pre>
{{< /rawhtml >}}

