---
title: Brittle Crown
image: /img/Brittle_Crown.png
tags:
- Utility Items
---

{{< figure src="/img/Brittle_Crown.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">A wretched carnival.

They were doomed for good reason. Dunepeople of Aphelia: lost, in fanatic worship of parasitic influences. Lemurians: destined to a dead planet, picked clean. Chitin beasts. Automations of death. Why do you bring them home? They were not meant to survive. 

I have watched you for ages, from my dead rock - and every century, you disgust me with vanity. You invite vermin into your home. Wretches. Rats. Monsters. Creatures without restraint. Each and every one, planet killers. And yet, you entertain them as guests. Like children, requiring saving and protection.

She should have died for me. Her gift was wasted on you.

And when will we open discussion - dear brother - of all your thin lies? Why do you forbid your guests to leave? To pilot? Why do you fashion great walls and gates? Why do you weave constructs of destruction, if your role is protection? They are entries in your collection. You slaver. Gatekeeper. Hoarder.

Your death is fated. When you die - and you WILL die - I will be ready. I have been patient for millennia. That planet... is mine.</pre>
{{< /rawhtml >}}

