---
title: Ocular HUD
image: /img/Ocular_HUD.png
tags:
- Equipment
---

{{< figure src="/img/Ocular_HUD.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Ocular HUD
Tracking Number: 871*****
Estimated Delivery: 09/06/2056
Shipping Method: Standard
Shipping Address: Greivenkamp, 5th Houston St, Prism Tower, Earth
Shipping Details:

I wish you hadn't asked me for help. I was contacted by [REDACTED] and they explained... well, some things. Using their instructions, I was able to design this interface for prolonged exactness. The beauty of it all is that it will compound with any previous precision enhancing tools. Digital plus optical is the way to go.

While I still don't know everything, I feel like I'm already in too deep. You won't hear from me anymore after this.</pre>
{{< /rawhtml >}}

