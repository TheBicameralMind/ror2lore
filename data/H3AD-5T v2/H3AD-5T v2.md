---
title: H3AD-5T v2
image: /img/H3AD-5T_v2.png
tags:
- Damage Items
---

{{< figure src="/img/H3AD-5T_v2.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Denizens of lower-gravity planets like Mercury historically have had trouble adjusting to life on other planets. Mercurians, with their lower bone density (left) would typically shatter their femur within a week on heavier planets. Members of asteroid-rigged flotillas and colony ships have similar issues when arriving home from space.

The invention of Kinetic Dispersion Rings (above) help alleviate the stress of heavier-than-normal gravity by converting kinetic energy into heat, sound, and light. Settlers from Mercury could finally leave their home planet in safety."

-Mercurian History Museum</pre>
{{< /rawhtml >}}

