---
title: Runald's Band
image: /img/Runald's_Band.png
tags:
- Damage Items
---

{{< figure src="/img/Runald's_Band.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Should memories be lost,
Should oceans be clouded,
Will you bring me fervor?
Will you die with me?"

-The Syzygy of Io and Europa</pre>
{{< /rawhtml >}}

