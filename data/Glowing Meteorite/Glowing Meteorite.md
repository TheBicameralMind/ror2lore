---
title: Glowing Meteorite
image: /img/Glowing_Meteorite.png
tags:
- Equipment
---

{{< figure src="/img/Glowing_Meteorite.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">What a... peculiar piece of the stars that serendipity has brought us. I'm sure you can make more. The ratios are simple. It should be quite fun.</pre>
{{< /rawhtml >}}

