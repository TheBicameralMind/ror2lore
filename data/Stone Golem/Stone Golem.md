---
title: Stone Golem
image: /img/Stone_Golem_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Stone_Golem_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">“Many scholars have written of a lifeforce present in the ground we walk on. That the earth under our feet is alive, in its own way. After my experiences on this planet, I can contribute to that theory; Petrichor V is home to Golems, each towering over humans at roughly 10 feet high, and comprised of stone and twine and animated by mysterious, otherworldly forces. 

From basic observation, the Golems seem to act as a defense garrison for Petrichor. While gentle with inhabitants of the planet, their true power emerges when they enter the battlefield. Manipulating a potent energy source, the Golems attack with powerful shockwaves and laser beams capable of blasting away cliff faces with ease. At the time of writing, the mechanisms that animate the Golems are still unknown.”

- The Mysteries of Petrichor V, First Draft</pre>
{{< /rawhtml >}}

