---
title: Berzerker's Pauldron
image: /img/Berzerker's_Pauldron.png
tags:
- Damage Items
---

{{< figure src="/img/Berzerker's_Pauldron.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Antique Pauldron
Tracking Number: 58***********
Estimated Delivery: 04/05/2056
Shipping Method:  Priority
Shipping Address: Jungle VII, Museum of 2019, Earth
Shipping Details: 

Another antique for the collection. This bad boy was found on the battlefield where much of the War was fought. The excavation site was littered with bones, all surrounding the remains of one rebel soldier, who was carrying this artifact. According to hearsay and rumors, rebel soldiers wearing pauldrons much like this one would enter trances on the battlefield. Time would slow down, and all they could see was the enemy.

Of course, it’s just speculation, but… There were a lot of bodies surrounding this thing’s old owner. Be careful, OK?</pre>
{{< /rawhtml >}}

