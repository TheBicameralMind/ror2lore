---
title: Wake of Vultures
image: /img/Wake_of_Vultures.png
tags:
- Damage Items
---

{{< figure src="/img/Wake_of_Vultures.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Unique Leather Belt
Tracking Number: 40***********
Estimated Delivery: 10/23/2056
Shipping Method:  High Priority / Biological
Shipping Address: Auckland, New Zealand, Earth
Shipping Details: 

“The mind rules over its body from a fortress of bone, learning of the world around it through fleshy portals. The heart is just an extension of the body, which finds its root in your head.” 

Somebody said this, I can’t remember who. Anyway, make sure to take great care of it. It’s incredibly rare.</pre>
{{< /rawhtml >}}

