---
title: Interstellar Desk Plant
image: /img/Interstellar_Desk_Plant.png
tags:
- Healing Items
---

{{< figure src="/img/Interstellar_Desk_Plant.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: "Interstellar Plant Seed"
Tracking Number: 44******
Estimated Delivery: 08/27/2056
Shipping Method:  Standard/Biological
Shipping Address: Giardina Dealership, Naples, Earth
Shipping Details:

Hello - yes, I’d like one of those space plant seeds for the office. I think it would look so nice in here; it'd really brighten up the room, and it could really use it. Al at the Denver branch says he got one, and it's really made a difference. His sales are OFF the charts, and he's contributing all his success to this magic plant!

I've also heard that it sorta gives off a glow that has medical benefits. That would be great for the pain I've been having in my ankle after that run. Might even help the attitude of some of my customers.</pre>
{{< /rawhtml >}}

