---
title: Executive Card
image: /img/Executive_Card.png
tags:
- Equipment
---

{{< figure src="/img/Executive_Card.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

"When you're on the planet, keep a look out for any loose UES Security Chests. Orders from the higher ups, we're to retrieve any and all parcels that were aboard the Contact Light and bring 'em home. When you locate them, open the Security Chest and retrieve the item within. When you've verified the contents of the chest, move out and continue with your other assignments. Understood?"

"Sir yes sir! But how are we going to open them? They're locked tight, aren't they?"

"A good question, soldier. Everyone, take one Executive Credit Card, then pass the tray to your next squad mate."

"E-Executive cards...? Aren't these only used by like, super rich folks? How are we ever going to pay it back?"

"What? No. You don't pay it back."

"But aren't we borrowing money?"'

"Yes."

"So don't we have to pay it back, eventually?"

"Yes, with another card."

"Sir, I don't understand."

"No, you just uh... well you see, the way these cards work is that, uh... [?] well you can take out a low-interest loan to... the buying power of debt is...[??]"

<span class="mono">//<span>--</span>TRANSCRIPTION UNINTELLIGIBLE <span>--</span>//</span></pre>
{{< /rawhtml >}}

