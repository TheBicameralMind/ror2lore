---
title: Bighorn Bison
image: /img/Bighorn_Bison_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Bighorn_Bison_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">“The Saturnian Bison – commonly referred to as the Bighorn Bison of Rademia – is a mystery among zoologists due to their sudden and mysterious disappearance from their native habitat. 

While poaching is considered the current and most likely explanation, strict hunting laws on Saturn debunk this theory. The Bighorn Bison’s incredible strength and insulating coat made it a key predator among the Rademian ecosystem – and their strange, metallic growths made it a popular trophy for hunters. 

To this day, the vanishing of the Saturnian Bison remains as one of zoology’s greatest mysteries.”

- Xenobiology of the Remus System, Second Edition</pre>
{{< /rawhtml >}}

