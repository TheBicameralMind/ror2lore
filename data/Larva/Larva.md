---
title: Larva
image: /img/Larva_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Larva_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!

</span>This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

We've just found a great example of the diverse lifestyles of outer life! Analyzing the life cycles of these creatures will certainly be a fun endeavor. I have assigned their common name as "Forhamian Acid Tick."

* The Ticks' biology is dominated by a bile sac, which not only stores acidic bile but also houses their eggs.

* When threatened, the Ticks produce bile at an accelerated rate, filling their bile sacs to burst - literally. I've noted some specimens capable of producing blasts of acid with a range of up to 3 meters in diameter.

* When they burst, their acid provides an avenue for newly hatched larvae to bury into prey and feast on their nutrient-rich meat.

* Their acute senses prevent predators from sneaking up on them. On top of this, the creatures exhibit automimicry, their false head spewing acidic bile onto would-be attackers, leaving enough time for the Tick to escape.</pre>
{{< /rawhtml >}}

