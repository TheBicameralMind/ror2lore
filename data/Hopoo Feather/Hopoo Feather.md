---
title: Hopoo Feather
image: /img/Hopoo_Feather.png
tags:
- Utility Items
---

{{< figure src="/img/Hopoo_Feather.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“...Europan laws have cracked down heavily on poaching, with the intent to help the reemergence of the hopoo. Since then, the Greebokks Foundation has helped support hopoo conservation groups, substantially growing their population beyond the initial three that were recorded in recent years.”

- Europan Wildlife Guide</pre>
{{< /rawhtml >}}

