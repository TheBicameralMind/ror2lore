---
title: Lunar Chimera (Exploder)
image: /img/Lunar_Chimera_(Exploder)_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Lunar_Chimera_(Exploder)_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">Production driven. Powered by refuse – all material has value.

Simple designs. Simple interference. Simple locomotion. 

Quality of components reduces stability. I will tie destruction with destruction.</pre>
{{< /rawhtml >}}

