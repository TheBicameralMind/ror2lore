---
title: Needletick
image: /img/Needletick.png
tags:
- Damage Items
---

{{< figure src="/img/Needletick.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete</span>

The following has been flagged as evidence for use in the trial of Titan v. Cooper, to be presided over by the Supreme Court of Titan. Some details may be expunged for the safety and confidentiality of included persons.

<span>---</span>

"Ah... |||||, my dear, do you watch any cartoons?"

"P-Please... I w-w-want to go h-h-home..."

"You see, there's this little trick they do that I've been meaning to try. You'll help me, won't you...?"

"Oh, ||||||||... help me..."

"Wonderful. Now stay nice and still for me. Let's see if this works..."

"W-What are you... Uh..."

"Give it a moment, |||||, my dear. Let me just put this darling toy back in its scabbard."

"W-What are you<span>--</span> OH GOD!!! HELP ME!!! PLEASE STOP, MY ORGANS<span>--</span>"

"Hey, no more tears. I just did something very cool. Let's see if I can do it with our favorite knife, hm?"

"PLEASE, NO, PLEASE PLEASE PLEASE"

The remainder of this transcript has been sealed.
Reason: vomit in buckets</pre>
{{< /rawhtml >}}

