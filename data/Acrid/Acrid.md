---
title: Acrid
image: /img/Acrid.png
tags:
- Survivors
---

{{< figure src="/img/Acrid.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Theoretically, the true contents of the cell were just a mess of atoms. The structure of the contents in each cell – living, weapon, whatever - was categorized, sequenced, and then turned to juice and cured into solid gems. An incredibly efficient and lossless format.

And yet when the cell containment broke – and emergency protocols re-sequenced that mess of atoms back into the creature known as Acrid – he already knew. Maybe he was somehow conscious, in his juice-gem state. Maybe they injected him with information. Maybe he was connected to their psionic network. Maybe he just made it up.

But he knew – that the strange crustaceans that scrambled his atoms and imprisoned him here, in a place unknown – were panicking. Something had gone wrong, in a world where variables were not allowed.

Something was <i>leaking</i>.

They may have finally imprisoned something that could not be contained.</pre>
{{< /rawhtml >}}

