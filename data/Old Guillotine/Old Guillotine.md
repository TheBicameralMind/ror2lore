---
title: Old Guillotine
image: /img/Old_Guillotine.png
tags:
- Damage Items
---

{{< figure src="/img/Old_Guillotine.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Old Guillotine
Tracking Number: 782*****
Estimated Delivery: 04/29/2056
Shipping Method: Standard
Shipping Address: Warehouse 36, Anklar, Primas V 
Shipping Details:

Everyone is still operating on adrenaline here. We finally overthrew our oppressors and have taken back Primas V! I know some of the overlords will attempt to buy their way onto a stealth transport, but that’s going to be quite difficult due to their epic economic blunder.

We don’t just want blood for all the injustices we’ve suffered at their hands. We want to send a message to would-be sympathizers. This old guillotine will serve both as an execution method and a symbol to strike fear into their hearts wherever they might be hiding.

Primas V is alive!</pre>
{{< /rawhtml >}}

