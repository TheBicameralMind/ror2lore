---
title: Hardlight Afterburner
image: /img/Hardlight_Afterburner.png
tags:
- Utility Items
---

{{< figure src="/img/Hardlight_Afterburner.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Specimen XAS4-49
Tracking Number: 342******
Estimated Delivery: 12/29/2056
Shipping Method:  Priority
Shipping Address: Geshka Tower, 33 Floor, Mars
Shipping Details:

Our 'hard light' research has become even more refined since our last correspondence. 

The initial purpose of the afterburner was to function as a primary heatsink for our bigger HL implementations - like our bridges and barriers. However, if attached to a rapidly degrading source, like those we typically dispose, we get a wonderful emission rate of semi-tachyonic particles. In other words... extremely high capacity fueling.

It should be obvious by its design, but to reiterate: stay away from the HL exhaust end when active. The emission method is violent by design, and so should be mounted to static, STABLE sources only.</pre>
{{< /rawhtml >}}

