---
title: Bandolier
image: /img/Bandolier.png
tags:
- Utility Items
---

{{< figure src="/img/Bandolier.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Bandolier
Tracking Number: 112*****
Estimated Delivery: 04/19/2056
Shipping Method: Standard
Shipping Address: 3950 Sunsell Ln, Tri-City, Earth
Shipping Details:

Thank you for your participation in the auction! We’ve included a short history on the item, as well as documents to verify its authenticity.

This is the famous bandolier worn by B. Grundy himself. He and his pals used to raise terror all over the map of the new territories. Their favorite activity was to tie up people they didn’t like and drag them behind their horses – Grundy clearly had a sweet spot for the old days.

The sling carries an impressive assortment of ammunition - Grundy himself carried many different guns. It was rumored that they held out for 3 whole days and nights before their weapons ran dry.</pre>
{{< /rawhtml >}}

