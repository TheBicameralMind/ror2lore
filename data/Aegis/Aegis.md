---
title: Aegis
image: /img/Aegis.png
tags:
- Utility Items
---

{{< figure src="/img/Aegis.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Artifact E-8EE572
Tracking Number: 490******
Estimated Delivery: 08/10/2056
Shipping Method: Priority
Shipping Address: Titan Museum of History and Culture, Titan

Sorry about the delay, we've had a flood of orders come in from this site. But it was exactly where you said we should look - there was a sealed off room where you marked the excavation diagram. I finished translating the engraving too, so consider that a bonus for the time we took to get to it:

"I am the will to survive made manifest. To those who never lose hope, to they who try in the face of impossible odds, I offer not 
protection, but the means to bring one's unconquerable spirit forth as the defender of their mortal lives."

It’s so lightweight, we figure it must've been entirely decorative. That seems to line up with the text. In any case, I hope it makes a good exhibit! I'm a big fan of the museum, so it wouldn't hurt to give me a partner's discount next time I visit, right?</pre>
{{< /rawhtml >}}

