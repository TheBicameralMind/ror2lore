---
title: Weeping Fungus
image: /img/Weeping_Fungus.png
tags:
- Healing Items
---

{{< figure src="/img/Weeping_Fungus.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

"This sucks."

"You know, you could have something to eat if you just said the magic word..."

"No! I'm not having one of those $#@!ing mushrooms!"

"C'mon, they're harmless. In fact, better than harmless, they're<span>--</span>"

"Don't even start. And we don't even know that, remember how you got an infection after you first tried them!?"

"Common head cold."

"Whatever. My point being, those things probably changed your digestive tract so you could eat them, or some weird $#!&amp; like that. I'm not stooping to your level!"

"Well, you're more than welcome to ride your high road with THEM, Mr. Picky Eater. Or, you could stay in the safety of this little cave, and share a snack with me."

"I... I'd rather<span>--</span>"

ROAAAAAAAAAAAAAR!!

"...Oh my god, fine. Hand it over."</pre>
{{< /rawhtml >}}

