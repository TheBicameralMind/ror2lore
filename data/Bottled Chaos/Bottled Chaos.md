---
title: Bottled Chaos
image: /img/Bottled_Chaos.png
tags:
- Utility Items
---

{{< figure src="/img/Bottled_Chaos.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The one planet they couldn't take.

They had everything locked away in their cells.

Beasts, artifacts of power, gods, memories.

And yet, no matter how many agents they sent, none returned victorious... if they returned at all.

The lucky ones who did survive fled, nervously whispering of a crystalline blade stained with chaos.</pre>
{{< /rawhtml >}}

