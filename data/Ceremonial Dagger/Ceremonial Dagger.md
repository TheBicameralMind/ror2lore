---
title: Ceremonial Dagger
image: /img/Ceremonial_Dagger.png
tags:
- Damage Items
---

{{< figure src="/img/Ceremonial_Dagger.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The smell of sulfur fills the air.

"Oh god, they're so close. They have Hitchcock. Oh god."

"Grab the - the dagger. From my backpack. Quick."

"I-I don't... I got it. What the hell? This isn't gonna work, Marion. What the hell am I supposed to do with this against those...? They blew him into pieces - oh god."

"You have to... you have to kill me - ah! Kill me. Kill me with it."

"What? What the hell?"

"Yeah - it's the only way. Please."

"What are you saying? Stop!"

"It's a magic dagger. It'll save us. She will save us. But you have to kill me with it."

"Please don't... please don't make me. Oh god."

"Y-you have to. I've seen how this works. It's the only way - they're coming so close. You have to use it on me. Kill me."

"I'm so sorry Marion. I'm so so sorry. I don't think-"

"Do it. Do it now. DO IT NOW! DO IT NOW! DO IT-"</pre>
{{< /rawhtml >}}

