---
title: Predatory Instincts
image: /img/Predatory_Instincts.png
tags:
- Damage Items
---

{{< figure src="/img/Predatory_Instincts.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Gossip spreads quickly in terrified communities. Stories that keep the more fearful lizardfolk in their passageways. Hushed tales that frighten the most commanding of stone constructs and woodfolk and creatures of the plains. Fear travels between the trees and the valleys.

A demon, fallen from the Sky, mighty enough to slay Providence and his Wurms. Only two arms, two legs - but with 22 unblinking, crimson eyes.</pre>
{{< /rawhtml >}}

