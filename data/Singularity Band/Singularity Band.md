---
title: Singularity Band
image: /img/Singularity_Band.png
tags:
- Damage Items
---

{{< figure src="/img/Singularity_Band.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Whether light brightens or fades,
Whether space holds or buckles,
We will draw near.
We will end as one."

-The Syzygy of Io and Europa, Apocryphal Verse</pre>
{{< /rawhtml >}}

