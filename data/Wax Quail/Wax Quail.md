---
title: Wax Quail
image: /img/Wax_Quail.png
tags:
- Utility Items
---

{{< figure src="/img/Wax_Quail.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Wax Quail
Tracking Number: 15******
Estimated Delivery: 12/18/2056
Shipping Method: Priority
Shipping Address: Research Center, Polarity Zone, Neptune
Shipping Details:

Hello Buu,
How are you doing? This is Mama. 

The weather here has been dreadful. It is raining every day. We have recently found a new bakery nearby that has delicious sandwiches. Papa has recently taken up whittling. He uses wax because it is softer. I have attached one of his favorites. Hopefully it has not been dinged in the mail :-)

Anyways, I will leave Buu alone. Hope to see you soon!
Mama</pre>
{{< /rawhtml >}}

