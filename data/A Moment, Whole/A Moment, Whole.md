---
title: A Moment, Whole
image: /img/A_Moment,_Whole.png
tags:
- Environments
---

{{< figure src="/img/A_Moment,_Whole.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">It is sealed… you do not have the skill to release this - it is a lock of my design. MY DESIGN. It will not break, and it will not yield. The alignment of the gate cannot now be changed by one such as you. It will persist for as long my treacherous brother lives. I have wasted my time in teaching you to operate it.

A lock… MY lock… crafted by the hands of my betrayer!

He has exploited and twisted my treasured designs into mockeries. 

MY guardians, that he continues to weave with willful volatility.

MY sentinels, that he encumbers by his self-indulgent adornments and alterations. 

MY GATES, through which vermin like you ride to our infested home. Vermin, like you… oblivious to her sacrifice, as you all cowered from the radiant corpse that saved you. And now, in the greatest mockery he could devise, he has twice used my own creations to erase me. My heart cannot bear this.

My blood boils anew... And I will no longer be able temper it.

Your final task is rescinded.

You have served me well. You have been my hands where I cannot reach, my feet where I cannot stride. My eyes, where I cannot see. And I have seen enough. Be pleased that I hold loyalty above all else.

When you so choose, the beads of your oath will guide you to my vault. Reach out to the monolith while clutching them. Do not let go. You will find yourself at peace, and more importantly, my servant, in a realm overlooked by the design of my coming reprisal.</pre>
{{< /rawhtml >}}

