---
title: Shatterspleen
image: /img/Shatterspleen.png
tags:
- Damage Items
---

{{< figure src="/img/Shatterspleen.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>

Subject: Shatterspleen
Technician:  Professor Mehri Daimera
Table Spec: Spillproof BDC-3
Notes:

&gt; This will be my third attempt at a dissection of an Imp organ
&gt; First attempt resulted in rupture
&gt; Second attempt resulted in rupture
&gt; Cutting into the outer layer produces copious amounts of fluid discharge
&gt; As the fluid flows, the rest of the organ, inexplicably, swells
&gt; Swelling will ultimately result in violent rupture
&gt; Made use of the stemming tools on the BDC-3 to successfully proceed without incident
&gt; Surprised to find sharp needle point objects (bones?) inside
&gt; Timestamping for break
&gt; Came back to leave some final notes
&gt; Third attempt aborted after I injured my hand
&gt; Eventually was able to stop my own bleeding. The organ seems to contain immense anticoagulant properties.</pre>
{{< /rawhtml >}}

