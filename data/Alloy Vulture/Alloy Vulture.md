---
title: Alloy Vulture
image: /img/Alloy_Vulture_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Alloy_Vulture_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">&gt; Automated report 9f6d5d41671c241a13bac58b2482deb7 is now available from site record 741c655e2d36c5f6973fd3a36aa0f950.
&gt; Please refer to record 741c655e2d36c5f6973fd3a36aa0f950 for additional personnel details during your review.
&gt; Report Type: Transcription
&gt; - Source: 741c655e2d36c5f6973fd3a36aa0f950 (Personal Suit Recorder)
&gt; Priority: Medium
&gt; Report Content:

<span>--</span> Beginning of Excerpt Flagged for Review <span>--</span>

Karl: "STOP! Everybody stop!"

Astrid: "Karl? Why are you out of breath, did you /run/ all the way here? What's wrong?"

Emil: "What's going on?"

Karl: "YES, Astrid, and you both need to listen. You know those Hornets in the black combat suits that split off a while back? The top-dollar PMC badasses? I saw one of them take shots at those huge bird things we've seen sulking around the cliffs - and one of them SHOT HIM BACK. With a GUN. The bullets must've bounced off the hardened armor - but it still knew what to do. They're not dumb animals. No way."

Astrid: "That's… not possible."

Karl: "It is, it happened! They must've taken the guns from the last guys."

Emil: "Why did you run all the way here from your post just to tell us?"

Karl: "Because you didn't let me finish! After it ran the gun dry it just dropped it and swooped in to tear through his armor with its claws. Its CLAWS. It ripped him in half… and threw him off the cliff. Flew off to the peaks with his suit and his other half. They're at least as smart as us, worse tempered, and better armed. We can't keep taking the mountains, we've gotta backtrack and go around. And we need to pack - now."

Emil: "This has gotta be a [REDACTED] joke Karl. Right now?"

Karl: "YES. NOW. And cut the lights! Cut the radar and all radio comms too. Emil, send out one last call to Mikhail and Pavlo to get back here ASAP."

Astrid: "We can't do that; we'll be completely defenseless. That radar is the only warning we'll get!"

Karl: "Did I leave out the part about the robot? The [REDACTED] flying robot they have? The one that's been parked six meters in the air with an audience of those things? The one that they keep "talking" to – and has been staring straight at camp since I spotted it? They have a robot, Astrid, and it knows there's something here. We. Have. To. Go."

Emil: "This place is a nightmare. How could we only just be finding out about it now?"

Karl: "EMIL. Shut up and make the call! We can continue this conversation /after/ we're sure we've gone totally dark and after we're far, far away from here."

<span>--</span> End of Recording <span>--</span>

<span>--</span> End of Excerpt Flagged for Review <span>--</span>

&gt; Please refer to report c31653d0e4193db1d2ac281ee3828280 for full audio excerpt.</pre>
{{< /rawhtml >}}

