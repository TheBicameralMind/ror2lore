---
title: Gup
image: /img/Gup_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Gup_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!

</span>This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

While out on a routine surveillance mission, Tharson began reporting an aroma wafting around the area, smelling similarly to earth strawberries. Following the scent brought us to a large gelatinous mass, which seemed to sense our approach and lurched forward towards us. I have assigned their common name as "Gup."

* Seems to be a largely single-celled organism, similar to the Earth Caulerpa taxifolia. When endangered, it can quickly perform mitosis to split into multiple smaller, cognizant beings.

* I had Tharson tackle the creature to test its viscosity. Tharson, upon impact, was stuck to the beast and was briefly submerged in the Gup's slime. Tharson was left mostly unscathed from this encounter, though is reporting signs of dizzyness.

* The Gup's slime is intensely aromatic, smelling much like strawberry and other fruits. Could this be a hunting tactic, luring prey in, only for them to be stuck in the Gup's slimy trap?

* I had Tharson taste-test the Gup's slime, for science. Tharson reported an intensely bitter flavor, despite the pleasant aroma. Tharson is vomiting after this encounter, but is largely OK.</pre>
{{< /rawhtml >}}

