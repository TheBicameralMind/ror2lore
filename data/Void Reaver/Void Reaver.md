---
title: Void Reaver
image: /img/Void_Reaver_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Void_Reaver_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">I saw something last night on my post.

A giant... crab. It came out of thin air and started tapping along the perimeter with one of its forelegs - like it was checking for an opening. It'd check a few meters at a time, and then disappear. Again, and again.

So, I figured out its pattern.

I waited for it, where it would appear next. My rifle was aimed at empty space - and then suddenly, that empty space became a target. But I couldn't pull the trigger. 

It turned to look straight at me, and I was paralyzed. It wasn't fear, it was something else. It was talking into my brain. The words it used weren't any language I knew, but it compelled me to freeze. The experience wasn't something I can fully describe with words. I couldn't pull the trigger. I couldn't even reach for the radio. I couldn't turn to look away. While my head was ringing, it resumed its routine; I could only stand and watch. It finished its check, and then returned its attention to me, as though I were an afterthought. I could feel it take something from me, but I can't remember what it was. And then, it left.

My guess is that it got whatever it wanted, because it hasn't been back since.

Something is still wrong with my head. I can't even seem to work up the power to speak. Should I even tell anyone? Is knowing somehow worse?

And what about all those times I heard that same sound - before I ever came to this awful place?</pre>
{{< /rawhtml >}}

