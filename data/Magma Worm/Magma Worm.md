---
title: Magma Worm
image: /img/Magma_Worm_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Magma_Worm_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>
This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>
Upon inspection of a curious pronged artifact, we happened to awaken something living deep under our feet. After a brief quake, a massive serpentine creature, easily 50 feet long, erupted from the ground and arced through the air, knocking us off our feet. I will describe its properties below. I have assigned their common name as “Magma Worm”.

* Upon inspection of its (now dead) body, I discovered multiple ports along its skin. These ports must allow it to propel itself forward, releasing a gaseous exhaust that is then ignited to give the worm its fiery appearance.

* The size of the creature is something to behold. It must wield an incredible amount of energy, as the heat radiating off its body was enough to turn the earth it burrows through into smooth, glassy tunnels. It must either eat an incredible amount of food, or spend a long time hibernating under ground.

* The worm’s body is incredibly conductive, retaining temperatures of over 350 degrees Fahrenheit for over ten hours after the beast was felled.

* It appears that the worm is classically blind – what appear to be eyes are revealed to be symbiotes that grant the worm vision in exchange for protection against predators. How fascinating! I wonder how this relationship came about.</pre>
{{< /rawhtml >}}

