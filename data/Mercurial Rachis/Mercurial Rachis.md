---
title: Mercurial Rachis
image: /img/Mercurial_Rachis.png
tags:
- Utility Items
---

{{< figure src="/img/Mercurial_Rachis.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“The original information super-highway: the spine. A strait-laced path from the body to the brain, it is responsible for ensuring the survival of all life. However, its limitations involve being localized to its parent body. What if - just like the development of the inter-computer wireless network - one’s nervous system could communicate with another? What if the spine traced a path not just from body to mind, but a path that intersects between other bodies and minds?”

- Notes of Job Michaels, Serial Killer and Mad Scientist</pre>
{{< /rawhtml >}}

