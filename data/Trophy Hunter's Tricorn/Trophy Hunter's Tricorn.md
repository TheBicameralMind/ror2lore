---
title: Trophy Hunter's Tricorn
image: /img/Trophy_Hunter's_Tricorn.png
tags:
- Equipment
---

{{< figure src="/img/Trophy_Hunter's_Tricorn.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"I've always thought," said Rainsford, "that the Bighorn Bison is the most dangerous of all big game."

For a moment the general did not reply; he was smiling his curious red-lipped smile. Then he said slowly, "No. You are wrong, sir. The Bighorn Bison is not the most dangerous big game." He sipped his wine. "Here in my preserve on this planet," he said in the same slow tone, "I hunt more dangerous game."

"...Sir?" General Hadbury turned to Rainsford. "Tell me, Rainsford, what makes game... dangerous?" Rainsford thought for a moment. "...The, er, size, sir? Big animals are often pretty tough to take down." Habury nodded. "Indeed, size can be important, but that's not it. In my eye, what makes game dangerous is... intelligence." Hadbury met Rainsford's gaze with a twinkle in his eye.

"The best hunt is always a match of wits. Say I were to lay some traps - how would I go about disguising them so that my prize won't spot them from a mile away? It makes things so much more fun, you see." Hadbury turned, walking towards the railing of the balcony overlooking his reserve. "There's no fun to the hunt, or any game for that matter, if there is no risk involved... if there is no story to the prize. That is why, dear Rainsford, my halls are decorated with the spoils of hunts that could only be described as legendary." Hadbury's red-lipped smile grew into a red-lined grin. "Now get the men ready, for we are going to hunt."</pre>
{{< /rawhtml >}}

