---
title: Scorched Acres
image: /img/Scorched_Acres.png
tags:
- Environments
---

{{< figure src="/img/Scorched_Acres.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>

<span class="mono">DAY 03</span>
I’ve encountered a strange, alien compound. Several platforms, supported by large stone pillars. There are various tiers and “floors” decorated with plants and elaborate sculptures. The masonry of these ruins is incredible. 

Although they appear recently polished... it also seems uninhabited. Lucky me - I was getting tired and needed some rest. I’ll be setting up camp on a nearby cliffside, under the shade of one of the platforms.

<span class="mono">DAY 04</span>
I haven’t encountered anything yet, but I have heard strange sounds throughout the night. Animal calls, the low groaning of stone as it stretches, whispers on the wind... that last part might just be my imagination. 

I’ve taken to exploring the compound in my free time, trying to scrounge for resources. There’s an excess of food, plucked from exotic plants that appear to have been grown and cultivated. Yet, again, the compound is empty, apart from me.

<span class="mono">DAY 05</span>
I ran out of rations earlier. I’ve started using the planters located around the compound to grow myself some food – strange fruits that taste of... well, I can’t really describe the taste. I’m only just noticing the strange aromatic smell that permeates this compound... Smells sweet, but spicy at the same time.

<span class="mono">DAY 07</span>
I discovered some strange artifacts today. Masks! How novel. For some reason… I felt compelled to try a few on. As expected, they don’t fit at all – probably not meant for human use.</pre>
{{< /rawhtml >}}

