---
title: Clay Templar
image: /img/Clay_Templar_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Clay_Templar_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">Ducking behind a boulder, the survivor’s stomach sank as he heard the sounds of tar sloshing up the hill. From the sound of it, the Templar wasn’t alone – it had company.

He had left his shelter to scrounge for supplies – not that there was much to find in the desert – when he was assaulted by a new kind of monster. Resembling a man, the creature stood around seven to eight feet tall, with skin made of tar and elaborate accessories of clay decorating its body. 

He had had only a few moments to stifle his surprise as the Templar opened fire, spewing a barrage of tar pellets from the enormous pot it carried. The survivor ran, but the Templar scored several hits. The tar had seeped into his suit, and he was starting to lose sensation in his legs.

A brief spark of hope – the survivor had some medical supplies in his shelter. If he could only make it back, he may be able to treat his leg and remove the tar…

That spark was quickly extinguished in a hail of bullets.</pre>
{{< /rawhtml >}}

