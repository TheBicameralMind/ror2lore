---
title: Ignition Tank
image: /img/Ignition_Tank.png
tags:
- Damage Items
---

{{< figure src="/img/Ignition_Tank.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: 128 oz. Gas Tank
Tracking Number: 33******
Estimated Delivery: 05/8/2058
Shipping Method: Priority
Shipping Address: Death Valley, Earth
Shipping Details: Contains a cocktail of the galaxy's most flammable substances. Y'know, methane, ethylene, butane... A single spark would be more than enough to set this baby off!

Er, hold that thought. It spontaneously combusted while I was writing this. I'll get you a new one in case it burns during transit, this happens a lot!</pre>
{{< /rawhtml >}}

