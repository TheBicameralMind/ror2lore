---
title: Titanic Plains
image: /img/Titanic_Plains.png
tags:
- Environments
---

{{< figure src="/img/Titanic_Plains.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
Outputting local audio transcriptions...
</span>

I am recording this before I forget any details of what I just saw.

We set up Rallypoint Beta on a giant plateau of grass and rocks - we felt that the com-range was greater in a flat and wide area.  The plateau is littered with giant stone rings and arcs - hundreds of feet tall. We investigated them first after landing but we couldn't discern anything of interest.

We were making room for a com-center in our camp, and one of our MUL-T bots were in charge of clearing out some of the bigger boulders. It was easiest to just push them off the edge of the plateau. 

Not a minute later, we heard a deep rumble - and saw something unbelievable. It just... flew. One of the stone rings - a smaller one - began to groan and lift out of the earth, moving with unknown forces. And then it lifted into the air, slowly spinning. It hovered to a height of about 200 feet, and then stopped.

As it spun, something began to form in the center of the ring. Specks of dirt tumbled in the air, until it formed a pebble, then a small rock, then a boulder. An identical boulder to the one we just moved - created out of thin air. Despite being constructed only a moment ago, the boulder was worn and covered in moss and lichen like it has been around for centuries.

Then, the stone fell to the ground with an unceremonious crash - right on top of our coms relay.

Not too long after, the stone ring began to waver. Then, like the invisible strings holding it in place were cut, the ring also fell to the earth.  Crash. 

We stopped moving stuff around after that.

<span class="mono">Local audio transcriptions complete.</span></pre>
{{< /rawhtml >}}

