---
title: Beetle Queen
image: /img/Beetle_Queen_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Beetle_Queen_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>
This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>
Just like standard Earth insects, the Worker Beetles and Beetle Guards all serve a Queen, which we recently discovered during an archeological dig.

I will describe her properties below. I have assigned her common name as “Beetle Queen”.

* Just like other insect queens, the Beetle Queen dwarves her subjects. However, her size is much greater than it should be for an insect queen. Does this mean that the Queen herself hunts rather than sending her drones?

* The Beetle Queen issues orders via pheromone emission. Merely being in the presence of the Queen gave the air a sickly sweet aroma, and Tharson reported light-headedness and trouble thinking clearly.

* The Queen’s bile is incredibly acidic, clocking in at a whopping 0.05 on the pH scale. Immediately upon exposure to a test sample of steel, it began to dissolve and spew noxious fumes. Tharson is OK from this encounter.

* The Queen is able to summon unique Beetle specimens that can fly. These drones do not appear anywhere else on the planet – do these drones act as attendants to the Queen? Observing these flying drones is challenging – they only seem to last for a few seconds before expiring.</pre>
{{< /rawhtml >}}

