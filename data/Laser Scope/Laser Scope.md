---
title: Laser Scope
image: /img/Laser_Scope.png
tags:
- Damage Items
---

{{< figure src="/img/Laser_Scope.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Scope with Laser Addon
Tracking Number: 21******
Estimated Delivery: 03/22/2060
Shipping Method: Priority
Shipping Address: Royal Drive, Bubble Station, Mars
Shipping Details:

An improved model compared to my last shipment, this one has all the perks of the last model PLUS a fancy new laser pointer to help improve aim.

And, knowing you, it can blind your targets when you do those fancy acrobatics prior to firing. I gotta admit, that last video was pretty cool... Just don't let the union find out about it. You'll lose more than your license if they catch you playing with this.</pre>
{{< /rawhtml >}}

