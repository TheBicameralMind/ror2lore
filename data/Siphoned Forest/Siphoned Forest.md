---
title: Siphoned Forest
image: /img/Siphoned_Forest.png
tags:
- Environments
---

{{< figure src="/img/Siphoned_Forest.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!

</span>

"Um, this is the... log of Private June Marsh. I'm, uh, on a mission apart of the UES Safe Travels on a planet in Uncharted Territories... This is Day 1 of my stay in this... cold, wintery forest. I'm out here on scouting duties. My job is to report any xenobiology activity to the higher ups. But uh, heh, let's hope it doesn't come to that!"

"Um, Day 2 of the log, June's log. It's really cold out here... You'd think the trees would help break the breeze, but it's blowing daggers... Brr. At the very least, I got some warm meals here. Cocoa really hits the spot in a place like this. Bottoms up...!"

"Hi, it's my log - I think staying out here all by myself is doing things to my head. The snow drifts... They can't really be moving? It's just the wind blowing them around. R-Right...? I thought I saw something… s-someone… moving through the trees. Maybe I should report this, j-just in case..."

"Hahh... Hahh... It all happened so fast... lizards, big things of stone, dug their way out of the snow. While I was busy, they rushed me. Knocked me square in the chest with a blast and sent me flying... It's... hard to breathe. And it's cold. So cold."</pre>
{{< /rawhtml >}}

