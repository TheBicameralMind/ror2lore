---
title: Sawmerang
image: /img/Sawmerang.png
tags:
- Equipment
---

{{< figure src="/img/Sawmerang.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">I'm so happy they let me bring my trusty disc toy on this trip! Trees need cutting? Disc toy. Baddies getting a little too close? Disc toy. Fun after-hours game of catch? Disc toy... but only if you're trained to catch it.</pre>
{{< /rawhtml >}}

