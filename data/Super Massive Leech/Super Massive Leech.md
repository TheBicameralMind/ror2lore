---
title: Super Massive Leech
image: /img/Super_Massive_Leech.png
tags:
- Equipment
---

{{< figure src="/img/Super_Massive_Leech.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Return: Super Massive Leech
Tracking Number: 817*****
Estimated Delivery: 07/01/2056
Shipping Method: High Priority/Biological
Shipping Address: Rare/Extinct Study Satellite, Beachfront, Saturn
Shipping Details:

While the medical benefits of this creature are obvious, the psychological effects are not. Upon receiving a previous shipment, Carolyn took charge of research and experimentation with the organism. 

Everything was by the book until, one day, she walked into the lab with the specimen clearly attached to her neck. Incredibly startled, the rest of us eventually coaxed her into placing it back into containment.

We inspected the bite area but, after running all the standard tests, we did not observe any adverse side effects.

She stated that it made her feel fantastic, but no one else in the lab felt comfortable with her behavior.</pre>
{{< /rawhtml >}}

