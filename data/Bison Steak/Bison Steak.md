---
title: Bison Steak
image: /img/Bison_Steak.png
tags:
- Healing Items
---

{{< figure src="/img/Bison_Steak.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Assorted Saturnian Bison Steak, 10lbs
Tracking Number: 31***********
Estimated Delivery: 11/02/2056
Shipping Method:  Standard
Shipping Address: Sloppy Joe’s Deli and Catering, Manhattan, New York
Shipping Details: 

FOR: JOSEPH ******
CC#: **** **** * ***
ACCT#: 102215
Quality Saturnian Bison Meat [10lbs]
Treated with special antibiotics to ensure exceptional growth, shelf life, and texture.</pre>
{{< /rawhtml >}}

