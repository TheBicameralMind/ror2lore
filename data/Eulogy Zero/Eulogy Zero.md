---
title: Eulogy Zero
image: /img/Eulogy_Zero.png
tags:
- Utility Items
---

{{< figure src="/img/Eulogy_Zero.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">A eulogy for the victors of spiritual warfare:
This is a message from me to you.</span>

Though in life you will stumble, stammer and fall - though you will lose your loved ones and shed more tears than your eyes can muster - in time you can find peace on the planets you call home. A small creature waiting quietly for you, a smiling face from another stream, the silence as you breathe in the scent of a new season.
Let your heart remind you why you are here; let it tell you something new and something old. A mind remembers, but a heart knows.
I love me so that I can love you too -  I've wanted to disappear before, but I've always wanted to return to see you. The grass and the leaves, the warmth of the sun and the insects at night. 

I promised myself, and I promise you: 

A victory is the reward of cumulative loss, ever present as a light at the end of the tunnel. Even in death, there is the lingering promise of new life - forever doomed to repeat the cycle.

It's getting dark. If you get lost it's alright, for now. I hope you enjoy the shooting stars. Rest in peace.</pre>
{{< /rawhtml >}}

