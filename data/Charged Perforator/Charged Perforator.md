---
title: Charged Perforator
image: /img/Charged_Perforator.png
tags:
- Damage Items
---

{{< figure src="/img/Charged_Perforator.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>

Subject: Charged Perforator
Technician: Dema "Dembones" Brown
Table Spec: GEC-4
Notes:

&gt; Using GEC-4 due to electricity above safe levels.
&gt; Removing charged enamel and placing aside for substance analysis. It’s solid, yet swimming.
&gt; Upon structural investigation, found cavities and internal batteries
&gt; Reduce lab electricity by 10 kilowatts
&gt; Electron generating veins present - charge is being supplied to the tooth?
&gt; Removed my lab coat, very staticky
&gt; Electron generation is still occurring in the severed object
&gt; Put some more ice in my drink WOW it's electric
&gt; Timestamping for dance break</pre>
{{< /rawhtml >}}

