---
title: Regenerating Scrap
image: /img/Regenerating_Scrap.png
tags:
- Utility Items
---

{{< figure src="/img/Regenerating_Scrap.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

"Hey, Joe, how's the work in engineering?"

"Terrible. We have a shipment of this... weird, prototype material. Some kind of metal? They want us to make stuff out of it, which isn't too bad. Thing is, no matter how much I take, there always seems to be more. Did you know I made twenty-five hundred units of .300 caliber rounds from a 10 kilo crate of metal?"

"How much!?"

"Right!? I feel like I'm losing my mind. It's not even half-way empty. Hell, I bet there's more in there than when I started!"

"Well, at least you won't have to worry about running out..."</pre>
{{< /rawhtml >}}

