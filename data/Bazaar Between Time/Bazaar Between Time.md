---
title: Bazaar Between Time
image: /img/Bazaar_Between_Time.png
tags:
- Environments
---

{{< figure src="/img/Bazaar_Between_Time.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">There was a sudden flash of white, blue, and green as the pod of time newts, two hundred strong, burst through the walls of the occlusion and into the slipstream.

They relaxed as they re-entered their natural habitat. The currents of the slipstream were strong - but they were much stronger. With magnetic webs, the newts propelled themselves at amazing speeds. They all swam effortlessly and gracefully – all but one.

Fifty meters behind the pod, a lone newt struggled to keep up. With a defective left arm – atrophied and bent - he didn’t have the same capabilities as his brothers and sisters. 

The group ignored his struggles as they continued down the stream. Soon, the pod was completely out of sight – and he was all alone, in the darkness of the slipstream.

Slowly, a smile creeped onto the face of the blind, disfigured newt. Finally. Unbeknownst to his brethren, he knew of a secret place nearby – and he didn’t want them to find out. He dove deep, between two boulders, and into a long, dark tunnel.

He passed through the tunnel and surfaced on the other side. Compared to the roaring of the slipstream, this place was seemingly silent. His smile grew wider.

A tidepool. The tension in the newt’s body began to fade. It’s been a very long day, and he could really use the rest. The tidepool was completely removed from the currents of the slipstream – and from the abuse of his pod. 

He began to float along the surface. The newt has been here hundreds of times before, but each time he fell deeper and deeper in love. His mind drifted to his experiments – it felt like he was near a breakthrough with some of his tinctures. 

With his mind preoccupied, he didn’t notice some changes had taken place since his last visit. He didn’t notice that the water levels were a bit higher – and a bit warmer. He didn’t notice that all the flyfish were gone. He didn’t notice the blue glow.

He didn’t notice the long, pale arms, grasping in the depths below.</pre>
{{< /rawhtml >}}

