---
title: Monster Tooth
image: /img/Monster_Tooth.png
tags:
- Healing Items
---

{{< figure src="/img/Monster_Tooth.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM ROOM 5041C OF UES [Redacted] <span>--</span>//</span>

“Hey, nice duds! Where’d you grab those?”

“Oh, this old thing? I’ve been making it, actually.”

“Making it?”

“Yeah, well, I found the first tooth in one of the chests. But every time we go out on patrol, we typically run into more of those aliens. Once we take care of ‘em, I try to pick myself out a souvenir. Y’know, to add to my collection.”

“Wow, uh… Is that like, allowed? I feel like that’s not allowed.”

“Eh, what the captain doesn’t know won’t hurt him. Besides, it IS cool, right?”

“I guess so. I’ll be honest man, that’s creepy as hell… but hey, do you have any more string? I might start one of my own.”</pre>
{{< /rawhtml >}}

