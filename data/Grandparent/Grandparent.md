---
title: Grandparent
image: /img/Grandparent_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Grandparent_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing audio... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

Uhm… this is DS-9, here at rendezvous point 9. We seem to have lost connection with the other dropships.
We are holding, waiting for zeta squad at designated rendezvous. Wow, it’s so beautiful here.
…
Uhm… we are seeing a, uh… MASSIVE unknown entity at the landing site. It seems impaired, or unaware. Oh my god… I’ve never seen anything like it. Wow, amazing. It’s so bright. I can’t believe something like this exists in the world.
…
Still holding – this is the rendezvous point, right? We are still waiting for zeta squad. 
…
The unknown entity is uh, turning around. 
…
MAYDAY MAYDAY THIS IS DS-9 WE HAVE BEEN STRUCK BY AN UNKNOWN ENTITY MAYDAY MAYDAY RENDEZVOUS 9 IS NOT SAFE I REPEAT NOT</pre>
{{< /rawhtml >}}

