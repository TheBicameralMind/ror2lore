---
title: Pearl
image: /img/Pearl.png
tags:
- Utility Items
---

{{< figure src="/img/Pearl.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">His influence is dark.

I do not blame you for your curiosity… but cleanse yourself of it immediately. It will ruin you – and all you care about.</pre>
{{< /rawhtml >}}

