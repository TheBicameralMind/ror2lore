---
title: Chronobauble
image: /img/Chronobauble.png
tags:
- Utility Items
---

{{< figure src="/img/Chronobauble.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Chronobauble
Tracking Number: 99******
Estimated Delivery: 03/03/2066
Shipping Method: Priority
Shipping Address: 9042 Pvt. Drive, Yustik Plaza, Mercury
Shipping Details:

Weren't you just a kid? Summer break feels like it was just so recent - but it's been 10 years, hasn't it? Life moves faster and faster as you get older - less moments to remember, to cement you in time. I don't even remember being 25. How did I forget an entire year?

Anyways - I found this in a trinket shop on Mercury. A chronobauble - the seller said something about special relativity, real heavy distortions or something - I didn't really get it. It's supposed to slow down time around it. Right.

Anyways, I'm shipping this to myself - to arrive in 10 years. A gift, from me to me. A reminder to make memories, to slow down a bit. 10 years will pass so quick - try to remember them! You've got to put effort in life!</pre>
{{< /rawhtml >}}

