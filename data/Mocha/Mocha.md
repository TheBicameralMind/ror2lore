---
title: Mocha
image: /img/Mocha.png
tags:
- Damage Items
---

{{< figure src="/img/Mocha.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: To-Go Coffee Cup, 16 ounces
Tracking Number: 32******
Estimated Delivery: 05/04/2058
Shipping Method:  Standard
Shipping Address: Museum of Natural History, Ninten Island, Earth
Shipping Details:
My finest brew. Hope it doesn't spoil during transit. Remember to heat it back up to 176.23 degrees... that's when it's freshest. See you soon... Coo.</pre>
{{< /rawhtml >}}

