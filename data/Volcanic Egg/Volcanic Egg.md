---
title: Volcanic Egg
image: /img/Volcanic_Egg.png
tags:
- Equipment
---

{{< figure src="/img/Volcanic_Egg.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Europan Great Dragon Egg
Tracking Number: 00****************
Estimated Delivery: 10/04/2056
Shipping Method:  Biological / Fragile
Shipping Address: Xenobiology Wildlife Reserve, Titan
Shipping Details: 

Make sure to keep this thing nice and hot while it incubates. I know it may seem plenty hot already, but Great Dragons are creatures of flame – they live the stuff, breath the stuff. Oh, and please send me pictures of the little guy once it hatches. Great Dragons are always so cute when they first hatch.</pre>
{{< /rawhtml >}}

