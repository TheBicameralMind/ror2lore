---
title: Jellyfish
image: /img/Jellyfish_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Jellyfish_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

Today, we’ve seen our first form of outer life on this planet, merely a few hours after our untimely landing.

I will describe its properties below. I have assigned their common name as ‘Icarian Jellyfish’.

• Large, round invertebrate, about 2m in diameter, with two trailing tentacles. They shimmer a white-blue. They greatly resemble Medusozoa from Earth. 

• They seem to be experiencing neutral buoyancy in this planet’s atmosphere, causing them to float. I theorize this is due to a unique gas composition inside of their hull, which they expel to traverse.

• When observed, these Jellyfish seem to bathe in the sun, slowly rotating their bodies as they do so. They may be cold-blooded.

• By my request, Tharson approached a lone Jellyfish. After detecting his presence, it began to glow, quiver, and pulse before suddenly detonating with a brilliant flash and a loud bang, causing severe burns and temporary blindness to Tharson. I theorize that by exciting the gases inside of their hulls, these Jellyfish can ignite those gases and explode, scorching everything nearby. Tharson is OK from this encounter.</pre>
{{< /rawhtml >}}

