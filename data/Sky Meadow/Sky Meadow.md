---
title: Sky Meadow
image: /img/Sky_Meadow.png
tags:
- Environments
---

{{< figure src="/img/Sky_Meadow.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
Outputting local audio transcriptions...
</span>

I’m tired. Haven’t slept in days. My feet are heavy in my boots. I should turn back, but… this place, it’s keeping me awake, if only to pull me further into its serenity. The soft grass, unnatural. The glowing fauna that plays tricks on my eyes. I’ve seen no predators, but what if they’re hiding…. waiting for me to take a rest?

The next plateau will help me find a landmark… no… but the one I can see from here might. These dangerous rock rivers I keep stumbling across, I’d stay away from them if they weren’t so fascinating. How do they even work? How can local gravity be so discrete? Maybe this is all a dream?

A gate! Yes, a stone gate! If I can figure out how to open it maybe I can find a place to finally rest! I'm so tired… no! I’ll sleep when I’m dead. The crowbar might not be enough for these massive doors. Ah, just my luck! 

Wait… what was that noise?</pre>
{{< /rawhtml >}}

