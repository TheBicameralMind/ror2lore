---
title: Personal Shield Generator
image: /img/Personal_Shield_Generator.png
tags:
- Utility Items
---

{{< figure src="/img/Personal_Shield_Generator.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"While the kinetic rating is very poor, the extremely cheap production costs of Hinon’s Personal Barrier made it a great budget tool for hikers, sailors, and even daily commuters to be comfortable in harsh weather. Many people in the office figured out they could replace their expensive winter jackets with comfortable, trendy personal barriers!"

-Top 10 Best Personal Barriers of 2053</pre>
{{< /rawhtml >}}

