---
title: Pluripotent Larva
image: /img/Pluripotent_Larva.png
tags:
- Utility Items
---

{{< figure src="/img/Pluripotent_Larva.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Life finds a way. Even in the depths of creation, there's bound to be something crawling along the sand, just hoping to make it through the day.

Why don't you take a ride in their shoes, just for one day? You may come out of the experience with a brand new perspective."</pre>
{{< /rawhtml >}}

