---
title: Parent
image: /img/Parent_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Parent_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

While assessing our landing site and the small ghostly creatures that now lay, unfortunately flat, under our ship, we heard loud booming footsteps approaching. We stepped away to observe this lifeform’s behavior from afar.

I will describe its properties below. I have assigned their common name as ‘Parent’.

• Tall, lumbering giants, eerily humanoid. Where you would expect to see a face there is only a hole, giving a blank look to them. Their ghostly skin appears to run like wax but even so it does not seem to congeal at their feet.

• Their large hands have opposable thumbs. This could possibly mean that they have the capability for intelligence, using tools or testing their environment with dextrous manipulation.

• If we hadn’t taken up this vantage point when we did, the creature would have been upon us before we realized it. Its distant footstep sounds betrayed how quickly it closed the distance.

• I had Tharson throw a stone near the creature and suddenly any signs of intelligence faded, replaced with primal fury. Deadly, wild force rained down as it smashed the ground with its fists, searching for the perpetrator of its peaceful moment. Our ship was not OK from this encounter.</pre>
{{< /rawhtml >}}

