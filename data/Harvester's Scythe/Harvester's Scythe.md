---
title: Harvester's Scythe
image: /img/Harvester's_Scythe.png
tags:
- Healing Items
---

{{< figure src="/img/Harvester's_Scythe.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“I’m not going nowhere. What I do is important, and no plague is going to stop me from getting that done. City folk are all up in a tizzy over this and that, scared that ya’ll will pass without doing some crazy goals you set for yourself. 

But me, I’m not afraid of death. Suffering is just another part of life, and like labor, I can do it myself. To death, I say: I don’t need your damn help.”

- Unnamed Farmer, Tragedy of Mercury: A History</pre>
{{< /rawhtml >}}

