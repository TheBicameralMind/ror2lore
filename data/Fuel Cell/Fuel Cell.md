---
title: Fuel Cell
image: /img/Fuel_Cell.png
tags:
- Utility Items
---

{{< figure src="/img/Fuel_Cell.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"As humanity began to venture out into the depths of space, high-energy but low-volume fuel sources became critical for interplanetary travel. Stability came later."
-Brief History of Interplanetary Advances, Vol.2</pre>
{{< /rawhtml >}}

