---
title: Defensive Microbots
image: /img/Defensive_Microbots.png
tags:
- Utility Items
---

{{< figure src="/img/Defensive_Microbots.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Captain, look out!” 

A voice cried from the old man’s periphery. But he knew he was in no danger – with a chirp, his companion microbots activated and fired, disintegrating the fireball. Just as it did every time. 

Quickly spinning on his heel, the old man fired his shotgun, decapitating the Lemurian with a swift and pronounced BOOM.

“I’d worry more about your own safety than mine, private.”</pre>
{{< /rawhtml >}}

