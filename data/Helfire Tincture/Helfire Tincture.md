---
title: Helfire Tincture
image: /img/Helfire_Tincture.png
tags:
- Equipment
---

{{< figure src="/img/Helfire_Tincture.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono"><span>========================================</span>
<span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>     [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;1453461 cycles&gt;
Complete!
Display result? Y/N
Y
<span>================================</span></span>
[Grix] ignites. We measure the time. 
.....
.....
.....
...
..
.

Too ordered. Composition is settling - I scrape the mixture from the bottom. 
[Ouju] ignites. We measure the time.
....
...
...
..
.

Too smooth. Ratio can be greater. I flatten the mixture. Black ichor of [Ouju] begins to pool around my feet. 
[Rhisko] ignites. I measure the time.
...
.

I increase the ratio of Tetrafoil. I saturate the mixture. Ichor floods the chamber. Ash flutters in the air before settling on the floor.
...
I ignite. &lt;He&gt; measures my time.
...
...
...
..
..
..
.

&lt;Perfect.&gt;
<span class="mono">
<span>================================</span></span></pre>
{{< /rawhtml >}}

