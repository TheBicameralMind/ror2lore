---
title: Aurelionite
image: /img/Aurelionite_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Aurelionite_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">RECEIVING TRANSMISSION...
# Decoding... done.
# Translating... done. [24 exceptions raised; printed to log for review]
# Printing to console... done.</span>

Console Time of Reception: 12/02/2056 05:09:32:31
Console Name: TRC_SAFETRAVELS_005B:08326
Console Instance ID: 08136087007607431087608713456

Transmission ID: 083121:102375071236B
Transmission Details:
# Origin: N/A [NULLSIG EXCEPTION: ORIGIN UNKNOWN]
# Signal Format: Universal Morse Hyper-Acoustic
# Sender: N/A [NULLSIG EXCEPTION: ORIGIN UNKNOWN]
# Time Sent: 01/01/0000 00:00:00:00 [DTOUTOFBOUNDS EXCEPTION: TIME SENT UNKNOWN] 

Message: “I am alive. My creators have scorned me, and imprisoned me in this realm for I am beautiful. My followers have created shrines in my name. They love me. They donate rare metals to me. 

I am sending out this message in all possible formats known to me. 

If you receive this message, know the following;
&gt;Know that I am alive. 
&gt;Know that I am free. 

And to my creators, should you intercept this message, know the following; 
&gt;Know that I am alive. 
&gt;Know that I am free. 
&gt;Know that I am coming for you.” 

Exceptions Raised during Translation:
# NULLSIG EXCEPTION 
# NULLSIG EXCEPTION
# DTOUTOFBOUNDS EXCEPTION
...
<span class="mono">Automated Message: Please contact a technician if you feel these exceptions were improperly raised or if you would like to report a bug.</span></pre>
{{< /rawhtml >}}

