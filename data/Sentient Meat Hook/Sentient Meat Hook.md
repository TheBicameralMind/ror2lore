---
title: Sentient Meat Hook
image: /img/Sentient_Meat_Hook.png
tags:
- Damage Items
---

{{< figure src="/img/Sentient_Meat_Hook.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"When artificial intelligence became more commonplace, many companies jumped on the trend for "smart" products. This included smart doors, lights, coffee makers, lawnmowers, vacuum cleaners, bedding, kitchen knife sets, and other home goods. After a series of gruesome lawsuits stemming from hacked smart appliances, most products soon went back to their more traditional, analog lines."

-"Why Is Smart So Dumb? VII"</pre>
{{< /rawhtml >}}

