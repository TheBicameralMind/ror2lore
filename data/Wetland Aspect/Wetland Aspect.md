---
title: Wetland Aspect
image: /img/Wetland_Aspect.png
tags:
- Environments
---

{{< figure src="/img/Wetland_Aspect.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
Outputting local audio transcriptions...
</span>

Yeah, I had to. I had no choice. Marion knew - and she was always so much stronger. So much stronger than all of us.

And it worked, it really did! Marion saved all of us. The dagger - it saved Pugh and Larsen and Dillon. Carney and Stevens.

But Pugh saw - he saw and he didn't understand. How could he? I tried to tell him - I ran over to him. But I couldn't convince him otherwise, and he was so crazy - so crazy! He wouldn't shut up and the golems were coming and the dagger - there was no choice.

Everyone was real somber that night. We lost Hitchcock and Marion and Pugh. Real somber. Later on, Carney found the book from a nearby chest. And it really all made sense - it did! I knew why Marion had that dagger and why I had to be the one to kill her. And Carney? He was too strong. I couldn't risk him not understanding. He wouldn't understand.

But Carney was big. Everyone else would've known. So that night, after I killed him, I went and took his body to the Altar. And I hid the Altar - right at the edge of the cliff, between two clusters of roots, in a cave underneath. And in that cave... I worshipped Her Concepts.

The rest? Well, they were so old and so tired. They were just gifts for N'kuhana. And She loved them. 

Weshan.
<span class="mono">
Local audio transcriptions complete.
</span></pre>
{{< /rawhtml >}}

