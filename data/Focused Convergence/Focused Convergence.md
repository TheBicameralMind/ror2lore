---
title: Focused Convergence
image: /img/Focused_Convergence.png
tags:
- Utility Items
---

{{< figure src="/img/Focused_Convergence.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Gravity concentrating... falling inward, condensing, hardening.

A singularity shrinks to the point your mind inverts, reversing reality. Now return. 

Don’t the matters of the physical world seem trivial now? Your tasks in this realm are simple and hastened, all in an attempt to get reversed. How many times can you make this trip before you are claimed by its process? 

Do not let it take you. Take it instead. Here.</pre>
{{< /rawhtml >}}

