---
title: Imp Overlord
image: /img/Imp_Overlord_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Imp_Overlord_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">&gt; Automated report 2c150f1430ad6430161e4 is now available from site record 1d20820aff461e471387e46b.
&gt; Please refer to record 1d20820aff461e471387e46b for additional details during your review.
&gt; Report Type: Machine-generated Transcription&gt; base Vernacular Profile “Geoff the Geockoff”
&gt; - Source: 1d20820aff461e471387e46b (Unknown Material Artifact #5821)
&gt; Priority: High
&gt; Report Content: 

<span>--</span> Beginning of Excerpt Flagged for Review –

LINE OPEN. [your eminence] SPEAKS.

IDENTIFY YOURSELF, SLUTS. AH, YES – YOU ARE A MEMBER OF THE RETRIEVAL SQUADRON. YOU ARE OVERDUE FOR A REPORT. HAVE YOU FOUND THE ARTIFACT?

WHAT? THE MISSION IS A COMPLETE FAILURE!? DO NOT SPEAK IN SUCH INSOLENT TONES.

I CARE NOT. YOUR DUTY IS TO [your eminence], FIRST AND FOREMOST. ALL EFFORTS MADE ARE TO FURTHER YOUR DUTY. EXCUSES ARE UNACCEPTABLE.

...A GUARDIAN, YOU SAY. VERY WELL. REINFORCEMENTS WILL BE DISPATCHED. WE ARE TRIANGULATING YOUR POSITION IN [the white plane]. HOLD POSITION.

TRIANGULATION COMPLETE. WE ARE MOVING OUT. I SHALL ACCOMPANY THEM – TO NOT ONLY ENSURE THERE ARE NO REPEATS OF YOUR SQUADRON’S INCOMPETENCE, BUT ALSO TO TEACH THIS GUARDIAN A LESSON. NONE SHALL RESIST [your eminence]’S WILL – NOT EVEN A PALTRY GOD. 

HAIL TO [the red plane]. [your eminence] OUT.

<span>--</span> End of Recording –

<span>--</span> End of Excerpt Flagged for Review –

&gt; TRANSLATION ERRORS: 3
&gt; 1&gt; [your eminence] could not be fully translated.
&gt; 2&gt; [the white plane] could not be fully translated.
&gt; 3&gt; [the red plane] could not be fully translated.

&gt; Please refer to report a13b2g8901cf2376102e for full audio excerpt.
<span>===================================================</span></pre>
{{< /rawhtml >}}

