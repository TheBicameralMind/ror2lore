---
title: Brilliant Behemoth
image: /img/Brilliant_Behemoth.png
tags:
- Damage Items
---

{{< figure src="/img/Brilliant_Behemoth.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Lasers have their speed. Swords have their edge. Gases have their reach. 

But let me tell you... Nothing, and I mean NOTHING, is as timeless as gunpowder. Just a pinch - jammed into a tight enough socket - and KABOOM! You got yourself not just a powerful weapon, but something that’ll leave a lasting sense of fear in your enemies. I mean, nobody wants to be blown up, right?”

- Autobiography of Jans Czar, Modern King of Weaponry</pre>
{{< /rawhtml >}}

