---
title: Preon Accumulator
image: /img/Preon_Accumulator.png
tags:
- Equipment
---

{{< figure src="/img/Preon_Accumulator.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Particle Accelerator Component
Tracking Number: 993******
Estimated Delivery: 06/06/2056
Shipping Method: Volatile
Shipping Address: Advance Particle Research Center, Mesquite, Earth

I still don't get how you keep losing so many accumulators. I build 'em tough, and they're supposed to last. I ran the numbers on the comm speeds, and I'm pretty sure you sent for this order less than two days after you should've got the last one. 

If this keeps up, you're gonna punch a hole in something where it doesn't belong. You've got a real good sponsor, Jim, but this stuff ain't cheap to ship. And it sure as heck ain't fast. So, I'm sending you a custom part. One that's as tough as I can make, and with a safety to keep you from overcharging it - that I'm sure you'll bypass anyway. 

At least try not to blow this one to bits for the first week, alright? Make our hometown look good. You're not gonna show up those fancy Europeans with a smoking scrap heap.</pre>
{{< /rawhtml >}}

