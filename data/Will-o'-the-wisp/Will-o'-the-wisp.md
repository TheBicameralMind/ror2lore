---
title: Will-o'-the-wisp
image: /img/Will-o'-the-wisp.png
tags:
- Damage Items
---

{{< figure src="/img/Will-o'-the-wisp.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Audio transcription complete from portable recorder.

Printing...</span>

"You don’t want to re-name it Bill-o-the-Wisp?"
"NO."
"JAR-ed?"
"NO."
"Then what about FLARE-ed?"
"STOP."
"It needs a name, it’s no dif-FIRE-ent than you or me."
"Next time we’re at base I’m applying for a transfer."
"You know that won’t go through until at least Dec-EMBER."
"How long have you been waiting to use these?"
"Coming up with them on the spot. I’ve got a good head on my SMOULDERS."
"You can stop emphasizing those words so much. I get it."
"I will never stop FUELING around."
"..."
"..."
"..."
"Want to grab something to eat when we get back?"
"Thank the stars that’s over, yes. I hope they’re not serving that gruel we had yesterday."
"Maybe they’ll cook you a pep-PYRO-ni pizza?"
"..."
"Hey, wait up! I can’t run holding this thing, it might go off!"</pre>
{{< /rawhtml >}}

