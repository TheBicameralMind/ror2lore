---
title: Remote Caffeinator
image: /img/Remote_Caffeinator.png
tags:
- Equipment
---

{{< figure src="/img/Remote_Caffeinator.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"I sure am thirsty," Chen said as he filled a Lemurian with bullets.

Luckily for him, he had found an Eclipse Remote Caffeinator aboard his vessel. Reaching in his pocket, Chen pressed the button marked with the Eclipse logo, and many systems began to whirr to life.

High up in the atmosphere, a panel on the hull of the UES Safe Travels slid open with a clunk. Ready to be ejected, an Eclipse Zero Vending Machine (c2038) twinkled valiantly. Silently, the vending machine began its journey down to the surface.

Back down on the planet, Chen was starting to run low on ammunition, and the Lemurians were closing in. But, just when the Lemurians were ready to tear Chen apart, who else but the Eclipse Zero Vending Machine came crashing down, squishing the Lemurians into a fine paste. Climbing up a mound of dead Lemurians, Chen entered 5 standard credits into the machine and ordered his favorite beverage, courtesy of the Eclipse Company. "Wow, refreshing and life-saving! Eclipse, quenching thirsts no matter where you are!" Chen smiled, grateful for Eclipse's galaxy-wide service and quality assurance.

<span class="mono">This log entry was brought to you by Eclipse: The Galactic Quencher</span></pre>
{{< /rawhtml >}}

