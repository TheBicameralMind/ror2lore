---
title: Royal Capacitor
image: /img/Royal_Capacitor.png
tags:
- Equipment
---

{{< figure src="/img/Royal_Capacitor.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: ZAP2-K “Royal” Capacitor
Tracking Number: 145***************
Estimated Delivery: 04/30/2056
Shipping Method:  Standard
Shipping Address: Power Plant 005, Route 10, Venus
Shipping Details: 

Here’s a replacement for your damaged equipment. Crazy how gang activity is starting up again – even crazier that they’d steal a capacitor from you guys. Anyway, you guys know the drill – make sure routinely discharge the capacitor’s storage cells, as it may hold on to latent voltage. Oh, and make sure not to point it at anybody when you do.

- The Saturn Reporter, Front Page Headline</pre>
{{< /rawhtml >}}

