---
title: Alien Head
image: /img/Alien_Head.png
tags:
- Utility Items
---

{{< figure src="/img/Alien_Head.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: "Alien Head"
Tracking Number: 76******
Estimated Delivery: 07/13/2056
Shipping Method:  High Priority/Biological
Shipping Address: Solomon, ???, Backwaters, Mars
Shipping Details:

Important sign from the spirits. Passed down for many generations. Must be used for high noon ritual to ward off sickness and fatigue. Must be kept cool or will rot. Helps energize the kinfolk and can be used for extra human strength and agility. Important.</pre>
{{< /rawhtml >}}

