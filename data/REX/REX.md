---
title: REX
image: /img/REX.png
tags:
- Survivors
---

{{< figure src="/img/REX.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM HYDROPONICS BAY OF UES [Redacted]<span>--</span>//</span>

“Status Report on Hydroponics Unit. Serial key begins with 12ea031lsd095a-“

“Yes, 12ea03. Tasked with care for the cabbage, lettuce, and mizuna growths. About 250 plants total. Stable, but losing biomass. I think there are issues with the microgravity. ”

“Keep an eye on the biomass, we might have to scrap 12ea03 and replace it with one of our evergreen Units. Moving on… serial key crp012d054jd0-“

“Crp01 is doing alright. Tasked with corn. About 80 plants total. Lost 6, down to 74.” 

“Sustainable, I suppose. What about… serial key rex0ch12d66m-“

“Oh, that Unit. Yeah, it was tasked with care for one of our cabbage hybrids. To be quite frank… it turned out terrible. I think we started with 80 units, but we’ve lost 79. The hybrid species just isn’t spliced right, I think. There was no way to sustain them.”

“And status report on the Unit?”

“Uh, well… it’s out on the star deck. It managed to hoist the last hybrid onto its platform base, and it’s been walking around the ship ever since, chasing down starlight. ”

“…What? The Unit left Hydroponics? 

“Yes, but-“

“So the Unit is disengaging from protocol?”

“Well, yes, but-“

“And instead of terminating the Unit, you let it roam around the ship unsupervised?”

“Just the team, well we just- we just kinda feel bad for the thing, you know. It’s trying really hard to keep that last hybrid alive, and-“

“Go terminate the Unit.”

“Well, the thing is- Rex is actually doing a great job! The hybrid’s biomass-“

“Are you kidding? Terminate the Unit. You have no idea what happens to a robot once it disengages from protocol. You are putting the life of your entire team in danger for the sake of a stray hydroponics unit. This should not even be a discussion.”

….

“Yes, sir.”
<span class="mono">
Transcriptions complete.
</span></pre>
{{< /rawhtml >}}

