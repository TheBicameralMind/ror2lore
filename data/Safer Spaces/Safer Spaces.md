---
title: Safer Spaces
image: /img/Safer_Spaces.png
tags:
- Utility Items
---

{{< figure src="/img/Safer_Spaces.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"You can't shoot me.

You can't taze me.

You can't gas me.

You can't even knock me down.

Not without something very precious going squish."</pre>
{{< /rawhtml >}}

