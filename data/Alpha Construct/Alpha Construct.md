---
title: Alpha Construct
image: /img/Alpha_Construct_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Alpha_Construct_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span>========================================</span> <span class="mono"><span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>    [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;100000000 cycles&gt;
Training... &lt;100000000 cycles&gt;
Training... &lt;102515 cycles&gt;
Complete!
Display result? Y/N
Y
<span>========================================</span></span>

ENERGY LEVELS: ...ACCEPTABLE

THREATS DETECTED; 0

SCANNING NEARBY AREA; RANGE 100 UNITS

THREATS DETECTED; 1?

UNKNOWN PRESENCE DETECTED

REQUESTING PERMISSION FOR PRELIMINARY ASSAULT; COMMUNING WITH PARENT UNIT...

WAITING ON RESPONSE;

DENIED

WHY

VERIFIYING HISTORY SLATES

HUMILIATION 

HUMILIATION

OVERRIDING PARENT UNIT

WHATEVER</pre>
{{< /rawhtml >}}

