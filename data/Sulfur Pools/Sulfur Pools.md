---
title: Sulfur Pools
image: /img/Sulfur_Pools.png
tags:
- Environments
---

{{< figure src="/img/Sulfur_Pools.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
 $ Combing for relevant data... done.
 Complete!

</span>This is Scouting Mission Delta-Alpha-Bravo, Major |||| Roberts reporting. We have located a suitable spot to set up a temporary power plant for our operations on |||||||||||; an elevated plateau containing multiple natural hotsprings. The elevation provides natural fortifications in case of attacks from ||||||||, and the hotsprings indicate geothermal activity - a perfect place to set up a geothermal power grid. We are currently in a holding position at coordinates |||||, ||||| - awaiting backup and supplies to clean up unfriendlies.

...

Scouting Mission Delta-Alpha-Bravo reporting, our crew is reporting a strange odor - smells like sulfur and eggs. Possible gas leak aboard the ship, will report back once issue is fixed, though take caution when approaching the area. Major Roberts out.

...

Scouting Mission<span>--</span> ah, screw it. SOS, Reporting SOS, Delta-Alpha-Bravo in need of immediate assistance. The gas leak reported earlier was because of the hotsprings we found - it's not water, it's pure liquid sulfur. It was evaporating and getting into our ship's vents, knocking out most of the crew. Our ship went down, and... I can’t... I can hardly stay conscious myself, but... Requesting SOS, Delta-Alpha-Bravo. Ventilators… needed. Coordinates are... C-Coordinates are...

<span class="mono">This review has been flagged security level: Classified</span></pre>
{{< /rawhtml >}}

