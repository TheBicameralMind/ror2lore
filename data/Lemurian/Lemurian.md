---
title: Lemurian
image: /img/Lemurian_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Lemurian_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">$ Transcribing image... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!

This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>

While on an expedition in search of edible substances to take back to the ship, we happened upon a lone creature who displayed remarkable intelligence. Though it was initially surprised at our sudden appearance, it quickly turned hostile - and Tharson was unfortunately forced to terminate it.

I will describe its properties below. I have assigned its common name as “Lemurian.”

* Tall, bipedal creatures resembling lizards. They are adorned in rudimentary armor of an unknown metal, showing signs of high intelligence.

* Capable of breathing flame, even managing to hit Tharson in the chest from a distance with a fire ball. Tharson is OK from this encounter.

* Appear to be warm blooded, unlike most Earthen lizards. I have observed them emerging from underground nests all across the planet, even in cold areas where lizards would not dare to dwell. The flame sac that produces their fire breath must be able to regulate their body temperature.

* When observed from a distance, the Lemurians are social creatures, usually hunting in packs and conversing amongst each other.  Their language is currently undeciphered, but it appears to be complex enough to support song and rhythmic flow.</pre>
{{< /rawhtml >}}

