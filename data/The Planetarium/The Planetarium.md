---
title: The Planetarium
image: /img/The_Planetarium.png
tags:
- Environments
---

{{< figure src="/img/The_Planetarium.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">I wake up again.

The simulacrum continues forever.

A million scenarios. A million permutations. It watches - calculating. Recording. Witnessing.

We always die. We have died millions of times. And every time, we are in the simulacrum.

We’ve escaped, once or twice. We defeated it – saw it die. Chased it down, through other worlds. We saw it collapse into a black hole. We got out, back to earth. Found a crashed vessel. It was rough, but we’ve done this scenario a few thousand times.

We spent time together, on Venus. We grew closer. Got married - got old together. We had kids – a boy and a girl. Erik had leukemia. That was hard for us.

Madelyn? She passed in her sleep. I followed on my own accord. I was satisfied.

But then I woke up again. In the simulacrum. It never happened. I was always in the simulacrum. Madelyn was from the simulacrum too. I think it liked that.

Am I still here?</pre>
{{< /rawhtml >}}

