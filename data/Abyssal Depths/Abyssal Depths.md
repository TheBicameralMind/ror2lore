---
title: Abyssal Depths
image: /img/Abyssal_Depths.png
tags:
- Environments
---

{{< figure src="/img/Abyssal_Depths.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
Outputting local plain text transcriptions...
</span>

<span class="mono">Day 0</span>

We have landed a few minutes ago and I have no time I must log as quickly as possible I hope the speech-to-text works many died on impact inside hollow cavern chains great chains fire the ground they come from the ground the burning they breathe fire we are hiding beneath the caverns groans they coax metal from the surface with song giant snakes made of fire they collect the security chests the death god we are in hell here chains keeps the ground from splintering they breathe beneath and above and around us punishments it must be forgive me I cannot see too dark deep thunder goodbye

<span class="mono">Local text transcriptions complete.</span></pre>
{{< /rawhtml >}}

