---
title: AtG Missile Mk. 1
image: /img/AtG_Missile_Mk._1.png
tags:
- Damage Items
---

{{< figure src="/img/AtG_Missile_Mk._1.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Fixing a spare bayonet onto his shotgun, he glanced at the horizon. The thundering of footsteps big and small was growing louder and louder – they were nearly upon him. He went over his kit one last time.

Five bayonets. Twenty three packs of incendiary explosives. Fifteen magazines of armor-piercing ammunition. Thirty three sticky bombs. Four tear-gas grenades, and so on.

And his favorite – two shoulder-mounted missile launchers, loaded with six AtG Viper Missiles. Heat seeking, detonation power of 15 pounds of TNT per missile. Light-weight, and the best part – automatic firing mechanism. He initially favored a more analog approach to his weapons, but the thing had grown on him.

Turning to face the oncoming mob, he loaded his shotgun. The adrenaline started pumping.

“Bring it on.”</pre>
{{< /rawhtml >}}

