#!/bin/bash

# set -eo pipefail

sudo apt-get update && sudo apt-get install -y bison

pip3 install requests bs4 tqdm jupyterlab

bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
. /home/vscode/.gvm/scripts/gvm

gvm install go1.21.4 -B
gvm use go1.21.4

CGO_ENABLED=1 go install -tags extended github.com/gohugoio/hugo@latest

echo "gvm use go1.21.4" >> ~/.bashrc
